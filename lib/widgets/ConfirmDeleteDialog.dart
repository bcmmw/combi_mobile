import 'package:combi_mobile/widgets/RoundedButton.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

confirmDelete(BuildContext context, String msg, Function onBtnPressed) {
  showDialog(
    context: context,
    builder: (context) => AlertDialog(
      title: Text(
        msg,
        style: GoogleFonts.openSans(
          textStyle: TextStyle(
            fontSize: 17,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      actions: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                RoundedButton(
                  btnText: 'Yes',
                  width: 100,
                  height: 50,
                  onBtnPressed: onBtnPressed,
                ),
                SizedBox(width: 8),
                RoundedButton(
                  btnText: 'No',
                  width: 100,
                  height: 50,
                  onBtnPressed: () {
                    Navigator.pop(context, false);
                  },
                ),
              ],
            ),
          ],
        ),
      ],
    ),
  );
}
