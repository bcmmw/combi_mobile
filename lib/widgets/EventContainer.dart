import 'package:cached_network_image/cached_network_image.dart';
import 'package:combi_mobile/controller/EventController.dart';
import 'package:combi_mobile/controller/UserController.dart';
import 'package:combi_mobile/models/Event.dart';
import 'package:combi_mobile/widgets/ConfirmDeleteDialog.dart';
import 'package:combi_mobile/widgets/RoundedButton.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:intl/intl.dart';

class EventContainer extends StatefulWidget {
  final Event event;
  final String currentUserId;
  final String teamId;

  const EventContainer({Key key, this.event, this.currentUserId, this.teamId})
      : super(key: key);
  @override
  _EventContainerState createState() => _EventContainerState();
}

class _EventContainerState extends State<EventContainer> {
  bool _isParticipated = false;
  //void getLeaderInfo() {}

  void initState() {
    super.initState();
  }

  _submit() async {
    int count = 0;

    await confirmDelete(context,
        'Confirm Join This Event? You must attend this event or else the points gained will be forfeited.',
        () {
      EventController.joinEvent(
          widget.teamId, widget.event.id, widget.currentUserId);
      // Update xp point and level
      UserController.levelUp(widget.event.point, widget.currentUserId);

      Navigator.pop(context, true);

      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Congratulations! You have gained ' +
                    widget.event.point.toString() +
                    ' points.',
                style: GoogleFonts.openSans(
                  textStyle: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
            ],
          ),
          actions: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                RoundedButton(
                    btnText: 'OK',
                    width: 100,
                    height: 50,
                    onBtnPressed: () =>
                        Navigator.of(context).popUntil((_) => count++ >= 1)),
              ],
            ),
          ],
        ),
      );
    });

    // EventController.joinEvent(widget.event.id, widget.currentUserId);
    // // Update xp point and level
    // UserController.levelUp(widget.event.point, widget.currentUserId);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        // GestureDetector(
        //   onTap: () => Navigator.push(
        //     context,
        //     MaterialPageRoute(
        //       builder: (_) => ProfileScreen(
        //         currentUserId: widget.currentTeamId,
        //         visitedUserId: widget.user.id,
        //         fromOtherPage: true,
        //       ),
        //     ),
        //   ),
        //   child:
        Container(
          padding: EdgeInsets.symmetric(
            horizontal: 16.0,
            vertical: 10.0,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.width / 6,
                    width: MediaQuery.of(context).size.width / 6,
                    //height: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                        image: CachedNetworkImageProvider(widget.event.image),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  // CircleAvatar(
                  //   radius: 70,
                  //   backgroundColor: Colors.grey,
                  //   backgroundImage: widget.event.image.isEmpty
                  //       ? AssetImage('assets/images/placeholder.png')
                  //       : CachedNetworkImageProvider(widget.event.image),
                  // ),
                  SizedBox(width: 8.0),

                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.event.name,
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                      ),
                      SizedBox(height: 10),
                      Row(
                        children: [
                          Icon(
                            Icons.event_note_sharp,
                            size: 15,
                          ),
                          Text(
                            '  ' + widget.event.description,
                            style: TextStyle(
                              fontSize: 15,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 5,
                          ),
                        ],
                      ),
                      SizedBox(height: 5),
                      Row(
                        children: [
                          Icon(
                            Icons.near_me_rounded,
                            size: 15,
                          ),
                          Text(
                            '  ' + widget.event.address,
                            style: TextStyle(
                              fontSize: 15,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 5,
                          ),
                        ],
                      ),
                      SizedBox(height: 5),
                      Row(
                        children: [
                          Icon(
                            Icons.access_time,
                            size: 15,
                          ),
                          Text(
                            '  ' +
                                DateFormat("yyyy-MM-dd HH:mm").format(
                                    widget.event.startDateTime.toDate()) +
                                ' - ' +
                                DateFormat("yyyy-MM-dd HH:mm")
                                    .format(widget.event.endDateTime.toDate()),
                            style: TextStyle(
                              fontSize: 15,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            '⭐ ' + widget.event.point.toString() + ' Points',
                            style: TextStyle(
                              fontSize: 15,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 5,
                          ),
                        ],
                      ),
                      SizedBox(height: 5),
                    ],
                  ),
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  // StreamBuilder(
                  //   stream: EventController.getParticipantsList(
                  //       widget.teamId, widget.event.id),
                  //   builder: (BuildContext context, AsyncSnapshot snapshot) {
                  //     if (!snapshot.hasData) {
                  //       return SizedBox.shrink();
                  //     }
                  //     List<Future<Member>> _particpants = snapshot.data;

                  //     return FutureBuilder(
                  //       future: snapshot.data,
                  //       builder:
                  //           (BuildContext context, AsyncSnapshot snapshot) {
                  //         if (!snapshot.hasData) {
                  //           return SizedBox.shrink();
                  //         }
                  //         List<Member> _particpants = snapshot.data;

                  //         if ((_particpants.firstWhere(
                  //                 (it) => it.id == widget.currentUserId,
                  //                 orElse: () => null)) !=
                  //             null) {
                  //           return RoundedButton(
                  //             btnText: 'Joined',
                  //             width: 50,
                  //             height: 50,
                  //             color: Colors.black,
                  //             onBtnPressed: null,
                  //           );
                  //         } else {
                  //           return RoundedButton(
                  //             btnText: 'Join Event',
                  //             width: 50,
                  //             height: 50,
                  //             color: Colors.black,
                  //             onBtnPressed: _submit,
                  //           );
                  //         }
                  //       },
                  //     );
                  //   },
                  // ),
                  widget.event.startDateTime.toDate().isAfter(DateTime.now())
                      ? StreamBuilder(
                          stream: EventController.getParticipantsId(
                              widget.teamId, widget.event.id),
                          builder:
                              (BuildContext context, AsyncSnapshot snapshot) {
                            if (!snapshot.hasData) {
                              return SizedBox.shrink();
                            }
                            List<String> _particpantsMemberId = snapshot.data;

                            if ((_particpantsMemberId.firstWhere(
                                    (it) => it == widget.currentUserId,
                                    orElse: () => null)) !=
                                null) {
                              return RoundedButton(
                                btnText: 'Joined',
                                width: 50,
                                height: 50,
                                color: Colors.black,
                                onBtnPressed: null,
                              );
                            } else {
                              return RoundedButton(
                                btnText: 'Join Event',
                                width: 50,
                                height: 50,
                                color: Colors.black,
                                onBtnPressed: _submit,
                              );
                            }
                            // _particpants.forEach((Future<Member> participant) {
                            //   return FutureBuilder(
                            //     future: participant,
                            //     builder:
                            //         (BuildContext context, AsyncSnapshot snapshot) {
                            //       if (!snapshot.hasData) {
                            //         return SizedBox.shrink();
                            //       } else {
                            //         Member a = snapshot.data;
                            //         if (a.id == widget.currentUserId) {
                            //           _isParticipated = true;
                            //         }
                            //         // _particpantsMember.add(a);

                            //         return SizedBox.shrink();
                            //       }
                            //     },
                            //   );
                            // });

                            // if (_isParticipated && _particpants.length > 0) {
                            //   return RoundedButton(
                            //     btnText: 'Joined',
                            //     width: 50,
                            //     height: 50,
                            //     color: Colors.black,
                            //     onBtnPressed: null,
                            //   );
                            // } else {
                            //   return RoundedButton(
                            //     btnText: 'Join Event',
                            //     width: 50,
                            //     height: 50,
                            //     color: Colors.black,
                            //     onBtnPressed: _submit,
                            //   );
                            // }
                            // if ((_particpantsMember.firstWhere(
                            //         (it) => it.id == widget.currentUserId,
                            //         orElse: () => null)) !=
                            //     null) {
                            //   return RoundedButton(
                            //     btnText: 'Joined',
                            //     width: 50,
                            //     height: 50,
                            //     color: Colors.black,
                            //     onBtnPressed: null,
                            //   );
                            // } else {
                            //   return RoundedButton(
                            //     btnText: 'Join Event',
                            //     width: 50,
                            //     height: 50,
                            //     color: Colors.black,
                            //     onBtnPressed: _submit,
                            //   );
                            // }
                          },
                        )
                      : SizedBox.shrink(),

//                    _particpants.forEach((Future<Member> participant) {

// });
                  //     RoundedButton(
                  //         btnText: 'Join Event',
                  //         width: 50,
                  //         height: 50,
                  //         color: Colors.black,
                  //         onBtnPressed: _submit),
                ],
              ),
            ],
          ),
        ),
        //),
        //SizedBox(height: 25),

        Divider(),
      ],
    );
  }
}
