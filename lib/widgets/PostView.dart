import 'dart:async';
import 'package:animator/animator.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:combi_mobile/controller/FeedController.dart';
import 'package:combi_mobile/models/Feed.dart';
import 'package:combi_mobile/models/Member.dart';
import 'package:combi_mobile/screens/screens.dart';
import 'package:combi_mobile/widgets/ConfirmDeleteDialog.dart';
import 'package:flutter/material.dart';

class PostView extends StatefulWidget {
  final String currentUserId;
  final Feed feed;
  final Member author;

  PostView({this.currentUserId, this.feed, this.author});

  @override
  _PostViewState createState() => _PostViewState();
}

class _PostViewState extends State<PostView> {
  int _likeCount = 0;
  int _commentCount = 0;
  bool _isLiked = false;
  bool _heartAnim = false;

  @override
  void initState() {
    super.initState();
    _likeCount = widget.feed.likeCount;
    _commentCount = widget.feed.commentCount;
    _initPostLiked();
  }

  @override
  void didUpdateWidget(PostView oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.feed.likeCount != widget.feed.likeCount) {
      _likeCount = widget.feed.likeCount;
    }
  }

  _initPostLiked() async {
    bool isLiked =
        await FeedController.isLikePost(widget.currentUserId, widget.feed);
    if (mounted) {
      setState(() {
        _isLiked = isLiked;
      });
    }
  }

  _likePost() {
    if (_isLiked) {
      // Unlike Post
      FeedController.unlikePost(widget.currentUserId, widget.feed);
      setState(() {
        _isLiked = false;
        _likeCount = _likeCount - 1;
      });
    } else {
      // Like Post
      FeedController.likePost(widget.currentUserId, widget.feed);
      setState(() {
        _heartAnim = true;
        _isLiked = true;
        _likeCount = _likeCount + 1;
      });
      Timer(Duration(milliseconds: 350), () {
        setState(() {
          _heartAnim = false;
        });
      });
    }
  }

  _toCommentScreen() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => CommentsScreen(
          currentUserId: widget.currentUserId,
          feed: widget.feed,
          likeCount: _likeCount,
          commentCount: _commentCount,
        ),
      ),
    ).then((value) => setState(() {
          _commentCount = value;
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        GestureDetector(
          onTap: () => Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => ProfileScreen(
                currentUserId: widget.currentUserId,
                visitedUserId: widget.feed.authorId,
                fromOtherPage: true,
              ),
            ),
          ),
          child: Container(
            padding: EdgeInsets.symmetric(
              horizontal: 16.0,
              vertical: 10.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: <Widget>[
                    CircleAvatar(
                      radius: 25.0,
                      backgroundColor: Colors.grey,
                      backgroundImage: widget.author.image.isEmpty
                          ? AssetImage('assets/images/placeholder.png')
                          : CachedNetworkImageProvider(widget.author.image),
                    ),
                    SizedBox(width: 8.0),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.author.name,
                          style: TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          '@' + widget.author.username,
                          style: TextStyle(
                            fontSize: 15,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                widget.currentUserId == widget.feed.authorId
                    ? Column(
                        children: [
                          PopupMenuButton(
                            icon: Icon(
                              Icons.more_vert,
                              color: Colors.black45,
                              size: 30,
                            ),
                            itemBuilder: (_) {
                              return <PopupMenuItem<String>>[
                                new PopupMenuItem(
                                  child: Text('Delete Post'),
                                  value: 'delete',
                                )
                              ];
                            },
                            onSelected: (selectedItem) {
                              if (selectedItem == 'delete') {
                                confirmDelete(context, 'Delete post?', () {
                                  FeedController.deletePost(feed: widget.feed);
                                  Navigator.pop(context, true);
                                });

                                super.setState(() {});
                              }
                            },
                          ),
                        ],
                      )
                    : SizedBox(),
              ],
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(
            horizontal: 16.0,
            vertical: 10.0,
          ),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Text(
                  widget.feed.caption,
                  style: TextStyle(
                    fontSize: 16.0,
                  ),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 15),
        GestureDetector(
          onDoubleTap: _likePost,
          child: widget.feed.imageUrl.isEmpty
              ? SizedBox.shrink()
              : Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Container(
                      //height: 250,
                      height: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        image: DecorationImage(
                          image:
                              CachedNetworkImageProvider(widget.feed.imageUrl),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    _heartAnim
                        ? Animator(
                            duration: Duration(milliseconds: 300),
                            tween: Tween(begin: 0.5, end: 1.4),
                            curve: Curves.elasticOut,
                            builder: (anim) => Transform.scale(
                              scale: anim.value,
                              child: Icon(
                                Icons.favorite,
                                size: 100.0,
                                color: Colors.red[400],
                              ),
                            ),
                          )
                        : SizedBox.shrink(),
                  ],
                ),
        ),
        SizedBox(height: 10),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: <Widget>[
                      IconButton(
                        icon: _isLiked
                            ? Icon(
                                Icons.favorite,
                                color: Colors.red,
                              )
                            : Icon(Icons.favorite_border),
                        iconSize: 30.0,
                        onPressed: _likePost,
                      ),
                      Text(
                        '${_likeCount.toString()}',
                        style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(width: 10),
                      IconButton(
                        icon: Icon(Icons.comment),
                        iconSize: 30.0,
                        onPressed: _toCommentScreen,
                      ),
                      GestureDetector(
                        onTap: _toCommentScreen,
                        child: Text(
                          '${_commentCount.toString()}',
                          style: TextStyle(
                            fontSize: 15.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Text(
                    widget.feed.timestamp.toDate().toString().substring(0, 19),
                    style: TextStyle(color: Colors.black54),
                  ),
                ],
              ),
              Divider(),
              SizedBox(height: 12.0),
            ],
          ),
        ),
      ],
    );
  }
}
