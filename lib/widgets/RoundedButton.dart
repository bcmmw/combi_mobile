import 'package:combi_mobile/utilities/styling.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class RoundedButton extends StatelessWidget {
  final String btnText;
  final Function onBtnPressed;
  final double fontSize;
  final double width;
  final double height;
  final Color color;

  const RoundedButton({
    Key key,
    this.btnText,
    this.onBtnPressed,
    this.fontSize = 15,
    this.width = 150,
    this.height = 60,
    this.color = Colors.white,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 5,
      color: kPrimaryColor,
      borderRadius: BorderRadius.circular(30),
      child: MaterialButton(
        onPressed: onBtnPressed,
        minWidth: width,
        height: height,
        child: Text(
          btnText,
          style: GoogleFonts.openSans(
            textStyle: TextStyle(
              fontSize: fontSize,
              fontWeight: FontWeight.w700,
              color: color,
            ),
          ),
        ),
      ),
    );
  }
}
