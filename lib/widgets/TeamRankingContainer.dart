import 'package:cached_network_image/cached_network_image.dart';
import 'package:combi_mobile/models/CombiTeam.dart';
import 'package:combi_mobile/services/levelExp_service.dart';
import 'package:combi_mobile/utilities/styling.dart';
import 'package:flutter/material.dart';

class TeamRankingContainer extends StatefulWidget {
  final CombiTeam team;
  final int rank;

  const TeamRankingContainer({Key key, this.team, this.rank}) : super(key: key);
  @override
  _TeamRankingContainerState createState() => _TeamRankingContainerState();
}

class _TeamRankingContainerState extends State<TeamRankingContainer> {
  double _currentSliderValue = 0;
  void initState() {
    super.initState();
    _currentSliderValue = widget.team.point.toDouble();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        // GestureDetector(
        //   onTap: () => Navigator.push(
        //     context,
        //     MaterialPageRoute(
        //       builder: (_) => ProfileScreen(
        //         currentUserId: widget.currentTeamId,
        //         visitedUserId: widget.user.id,
        //         fromOtherPage: true,
        //       ),
        //     ),
        //   ),
        //   child:
        Container(
          padding: EdgeInsets.symmetric(
            horizontal: 16.0,
            vertical: 10.0,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: <Widget>[
                  Text(
                    widget.rank.toString() + '. ',
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  CircleAvatar(
                    radius: 25.0,
                    backgroundColor: Colors.grey,
                    backgroundImage: widget.team.image.isEmpty
                        ? AssetImage('assets/images/placeholder.png')
                        : CachedNetworkImageProvider(widget.team.image),
                  ),
                  SizedBox(width: 8.0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.team.name,
                        style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      // Text(
                      //   '@' + widget.team.username,
                      //   style: TextStyle(
                      //     fontSize: 15,
                      //   ),
                      // ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
        //),
        SizedBox(height: 35),
        Container(
          transform: Matrix4.translationValues(0.0, -30.0, 0.0),
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '     Level ' + widget.team.level.toString(),
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      letterSpacing: 2,
                    ),
                  ),
                  Text(
                    widget.team.point.toString() +
                        '/' +
                        LevelExpService.calculateLevelExp(widget.team.level)
                            .toString(),
                    style: TextStyle(
                      fontSize: 13,
                    ),
                  ),
                ],
              ),
              Slider(
                value: _currentSliderValue,
                min: 0,
                max: LevelExpService.calculateLevelExp(widget.team.level)
                    .toDouble(),
                label: _currentSliderValue.toString(),
                activeColor: kPrimaryColor,
                inactiveColor: Colors.amber[100],
                onChanged: (double value) {
                  setState(() {
                    _currentSliderValue = value;
                  });
                },
              ),
            ],
          ),
        ),
        Divider(),
      ],
    );
  }
}
