import 'package:combi_mobile/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

alertDialog(BuildContext context, String msg, Function onBtnPressed) {
  showDialog(
    context: context,
    builder: (context) => AlertDialog(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            msg,
            style: GoogleFonts.openSans(
              textStyle: TextStyle(
                fontSize: 17,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
        ],
      ),
      actions: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RoundedButton(
                btnText: 'OK',
                width: 100,
                height: 50,
                onBtnPressed: onBtnPressed),
          ],
        ),
      ],
    ),
  );
}
