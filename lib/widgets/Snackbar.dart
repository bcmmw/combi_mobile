import 'package:flutter/material.dart';

snackBar(BuildContext context, String msg, Color color) {
  final snackBar = SnackBar(
    content: Text(msg),
    backgroundColor: color,
  );
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}
