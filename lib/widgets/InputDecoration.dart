import 'package:combi_mobile/utilities/styling.dart';
import 'package:flutter/material.dart';

InputDecoration inputDecoration(String label, IconData icon,
    {Widget suffixIcon = null}) {
  return InputDecoration(
    labelText: label,
    icon: Icon(
      icon,
      color: Colors.black87,
    ),
    labelStyle: TextStyle(color: Colors.black54),
    focusedBorder: UnderlineInputBorder(
      borderSide: BorderSide(color: kPrimaryColor),
    ),
    border: UnderlineInputBorder(
      borderSide: BorderSide(color: Colors.black54),
    ),
    suffixIcon: suffixIcon != null ? suffixIcon : null,
  );
}
