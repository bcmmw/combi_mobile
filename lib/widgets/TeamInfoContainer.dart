import 'package:cached_network_image/cached_network_image.dart';
import 'package:combi_mobile/controller/TeamController.dart';
import 'package:combi_mobile/controller/UserController.dart';
import 'package:combi_mobile/models/CombiTeam.dart';
import 'package:combi_mobile/services/levelExp_service.dart';
import 'package:combi_mobile/utilities/styling.dart';
import 'package:combi_mobile/widgets/RoundedButton.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TeamContainer extends StatefulWidget {
  final CombiTeam team;
  final String currentUserId;

  const TeamContainer({Key key, this.team, this.currentUserId})
      : super(key: key);
  @override
  _TeamContainerState createState() => _TeamContainerState();
}

class _TeamContainerState extends State<TeamContainer> {
  double _currentSliderValue = 0;
  int _pointGained = 5000;

  //void getLeaderInfo() {}

  void initState() {
    super.initState();
    _currentSliderValue = widget.team.point.toDouble();
  }

  _submit() async {
    int count = 0;
    TeamController.joinTeam(widget.team.id, widget.currentUserId);

    // Update xp point and level
    UserController.levelUp(_pointGained, widget.currentUserId);

    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Congratulations! You have gained $_pointGained points.',
              style: GoogleFonts.openSans(
                textStyle: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          ],
        ),
        actions: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RoundedButton(
                  btnText: 'OK',
                  width: 100,
                  height: 50,
                  onBtnPressed: () =>
                      Navigator.of(context).popUntil((_) => count++ >= 1)),
            ],
          ),
        ],
      ),
    );
  }
  // else {
  //   snackBar(context, 'Error: Please check again.', Colors.redAccent);
  // }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        // GestureDetector(
        //   onTap: () => Navigator.push(
        //     context,
        //     MaterialPageRoute(
        //       builder: (_) => ProfileScreen(
        //         currentUserId: widget.currentTeamId,
        //         visitedUserId: widget.user.id,
        //         fromOtherPage: true,
        //       ),
        //     ),
        //   ),
        //   child:
        Container(
          padding: EdgeInsets.symmetric(
            horizontal: 16.0,
            vertical: 10.0,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: <Widget>[
                  SizedBox(width: 8.0),
                  CircleAvatar(
                    radius: 25.0,
                    backgroundColor: Colors.grey,
                    backgroundImage: widget.team.image.isEmpty
                        ? AssetImage('assets/images/placeholder.png')
                        : CachedNetworkImageProvider(widget.team.image),
                  ),
                  SizedBox(width: 8.0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.team.name,
                        style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: 8.0),
                      FutureBuilder(
                        future: TeamController.getTeamMembers(widget.team.id),
                        builder:
                            (BuildContext context, AsyncSnapshot snapshot) {
                          if (!snapshot.hasData) {
                            return SizedBox.shrink();
                          }
                          int totalMembers = snapshot.data.length;
                          return Text(
                            totalMembers.toString() + ' member',
                            style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w400,
                            ),
                          );
                        },
                      )
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
        //),
        SizedBox(height: 25),
        Container(
          transform: Matrix4.translationValues(0.0, -25.0, 0.0),
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    ' Level ' + widget.team.level.toString(),
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      letterSpacing: 2,
                    ),
                  ),
                  Text(
                    widget.team.point.toString() +
                        '/' +
                        LevelExpService.calculateLevelExp(widget.team.level)
                            .toString(),
                    style: TextStyle(
                      fontSize: 13,
                    ),
                  ),
                ],
              ),
              Slider(
                value: _currentSliderValue,
                min: 0,
                max: LevelExpService.calculateLevelExp(widget.team.level)
                    .toDouble(),
                label: _currentSliderValue.toString(),
                activeColor: kPrimaryColor,
                inactiveColor: Colors.amber[100],
                onChanged: (double value) {
                  setState(() {
                    _currentSliderValue = value;
                  });
                },
              ),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text(
              'Join and get 5000 points',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w500,
                letterSpacing: 2,
                color: kPrimaryColor,
              ),
            ),
            SizedBox(width: 35),
            RoundedButton(
              btnText: 'JOIN NOW',
              width: 50,
              height: 50,
              color: Colors.black,
              onBtnPressed: _submit,
            ),
          ],
        ),
        Divider(),
      ],
    );
  }
}
