import 'package:cached_network_image/cached_network_image.dart';
import 'package:combi_mobile/controller/UserController.dart';
import 'package:combi_mobile/models/Event.dart';
import 'package:combi_mobile/utilities/styling.dart';
import 'package:combi_mobile/widgets/RoundedButton.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class EventContainer extends StatefulWidget {
  final Event event;
  final String currentUserId;

  const EventContainer({Key key, this.event, this.currentUserId})
      : super(key: key);
  @override
  _EventContainerState createState() => _EventContainerState();
}

class _EventContainerState extends State<EventContainer> {
  //void getLeaderInfo() {}

  void initState() {
    super.initState();
  }

  _submit() async {
    int count = 0;

    // EventController.joinEvent(widget.event.id, widget.currentUserId);
    // Update xp point and level
    UserController.levelUp(widget.event.point, widget.currentUserId);

    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Congratulations! You have gained ' +
                  widget.event.point.toString() +
                  ' points.',
              style: GoogleFonts.openSans(
                textStyle: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          ],
        ),
        actions: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RoundedButton(
                  btnText: 'OK',
                  width: 100,
                  height: 50,
                  onBtnPressed: () =>
                      Navigator.of(context).popUntil((_) => count++ >= 1)),
            ],
          ),
        ],
      ),
    );
  }
  // else {
  //   snackBar(context, 'Error: Please check again.', Colors.redAccent);
  // }

  @override
  Widget build(BuildContext context) {
    return Column(
      //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      //crossAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        // GestureDetector(
        //   onTap: () => Navigator.push(
        //     context,
        //     MaterialPageRoute(
        //       builder: (_) => ProfileScreen(
        //         currentUserId: widget.currentTeamId,
        //         visitedUserId: widget.user.id,
        //         fromOtherPage: true,
        //       ),
        //     ),
        //   ),
        //   child:
        Container(
          padding: EdgeInsets.symmetric(
            horizontal: 16.0,
            vertical: 10.0,
          ),
          child: Row(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.width / 3,
                    width: MediaQuery.of(context).size.width / 3,
                    //height: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                        image: CachedNetworkImageProvider(widget.event.image),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  // CircleAvatar(
                  //   radius: 70,
                  //   backgroundColor: Colors.grey,
                  //   backgroundImage: widget.event.image.isEmpty
                  //       ? AssetImage('assets/images/placeholder.png')
                  //       : CachedNetworkImageProvider(widget.event.image),
                  // ),
                  SizedBox(width: 8.0),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        children: [
                          Text(
                            widget.event.name,
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                          Text(
                            widget.event.description,
                            style: TextStyle(
                              fontSize: 15,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                        ],
                      ),
                      SizedBox(height: 15),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Join and get ' +
                                widget.event.point.toString() +
                                ' points',
                            style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 2,
                              color: kPrimaryColor,
                            ),
                          ),
                          SizedBox(width: 10),
                          RoundedButton(
                            btnText: 'View Details',
                            width: 50,
                            height: 50,
                            color: Colors.black,
                            onBtnPressed: _submit,
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
        //),
        //SizedBox(height: 25),

        Divider(),
      ],
    );
  }
}
