import 'package:combi_mobile/controller/FeedController.dart';
import 'package:combi_mobile/controller/UserController.dart';
import 'package:combi_mobile/models/Feed.dart';
import 'package:combi_mobile/models/Member.dart';
import 'package:combi_mobile/screens/CreatePostScreen.dart';
import 'package:combi_mobile/utilities/styling.dart';
import 'package:combi_mobile/widgets/PostView.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class FeedStreamBuilder extends StatefulWidget {
  final String currentUserId;
  final String visitedUserId;
  final bool allFeed;

  FeedStreamBuilder(
      {Key key, this.currentUserId, this.allFeed, this.visitedUserId})
      : super(key: key);

  @override
  _FeedStreamBuilderState createState() => _FeedStreamBuilderState();
}

class _FeedStreamBuilderState extends State<FeedStreamBuilder> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: widget.allFeed
          ? FeedController.getAllFeeds1()
          : FeedController.getUserFeeds1(
              widget.currentUserId == widget.visitedUserId
                  ? widget.currentUserId
                  : widget.visitedUserId),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) {
          return SizedBox.shrink();
        }
        final List<Feed> posts = snapshot.data;
        if (posts.length > 0) {
          return ListView.builder(
            shrinkWrap: true,
            physics: widget.allFeed
                ? BouncingScrollPhysics(
                    parent: AlwaysScrollableScrollPhysics(),
                  )
                : NeverScrollableScrollPhysics(),
            itemCount: posts.length,
            itemBuilder: (BuildContext context, int index) {
              Feed post = posts[index];
              return FutureBuilder(
                future: UserController.getUserWithId(post.authorId),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (!snapshot.hasData) {
                    return SizedBox.shrink();
                  }
                  Member author = snapshot.data;
                  return PostView(
                    currentUserId: widget.currentUserId,
                    feed: post,
                    author: author,
                  );
                },
              );
            },
          );
        } else {
          return Center(
            child: Column(
              children: [
                FloatingActionButton(
                  backgroundColor: kPrimaryColor,
                  child: const Icon(Icons.add),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => CreatePostScreen(
                            currentUserId: widget.currentUserId,
                          ),
                        ));
                  },
                ),
                SizedBox(height: 20),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25),
                  child: Text(
                    'Share your first post',
                    style: GoogleFonts.robotoSlab(
                      textStyle: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        }
      },
    );
  }
}
// class FeedStreamBuilder extends StatefulWidget {
//   const FeedStreamBuilder({
//     Key key,
//     @required this.currentUserId,
//     @required this.allFeed,
//   }) : super(key: key);

//   final String currentUserId;
//   final bool allFeed;

//   @override
//   Widget build(BuildContext context) {
   
//   }
// }
