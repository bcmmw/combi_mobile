import 'package:combi_mobile/models/Feed.dart';
import 'package:combi_mobile/utilities/constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class FeedController {
  static List<Feed> finalFeedList;

  static void createPost(Feed feed) {
    feedsRef.doc(feed.authorId).collection('userFeeds').add({
      'imageUrl': feed.imageUrl,
      'caption': feed.caption,
      'likeCount': feed.likeCount,
      'authorId': feed.authorId,
      'timestamp': feed.timestamp,
    });
  }

  static Future<List> getUserFeeds(String userId) async {
    QuerySnapshot userFeedsSnap = await feedsRef
        .doc(userId)
        .collection('userFeeds')
        .orderBy('timestamp', descending: true)
        .get();
    List<Feed> userTweets =
        userFeedsSnap.docs.map((doc) => Feed.fromDoc(doc)).toList();

    return userTweets;
  }

  static void CreateListofFeeds(QuerySnapshot snapshot) async {
    var docs = snapshot.docs;
    for (var doc in docs) {
      finalFeedList.add(Feed.fromDoc(doc));
    }
  }

  static Future<List> getAllFeeds(String userId) async {
    List list_of_uid = await feedsRef.get().then((val) => val.docs);

    for (int i = 0; i < list_of_uid.length; i++) {
      feedsRef
          .doc(list_of_uid[i].documentID.toString())
          .collection("userFeeds")
          .snapshots()
          .listen(CreateListofFeeds);

      return finalFeedList;
    }

    // QuerySnapshot userFeedsSnap =
    //     await feedsRef.get()
    //     ().getDocuments()
    // List<Feed> userTweets =
    //     userFeedsSnap.docs.map((doc) => Feed.fromDoc(doc)).toList();

    // return userTweets;
    // List<String> userIDs = [];
    // List<Feed> userTweets = [];
    // for (var i = 0; i < userIDsSnap.size; i++) {
    //   print(userIDsSnap.docs[i].toString());

    //   QuerySnapshot userFeedsSnap = await feedsRef
    //       .doc(userIDsSnap.docs[i].toString())
    //       .collection('userFeeds')
    //       .orderBy('timestamp', descending: true)
    //       .get();

    //   userTweets
    //       .addAll(userFeedsSnap.docs.map((doc) => Feed.fromDoc(doc)).toList());
  }

  static void likePost(String currentUserId, Feed feed) {
    DocumentReference feedDocProfile =
        feedsRef.doc(feed.authorId).collection('userFeeds').doc(feed.id);
    feedDocProfile.get().then((doc) {
      int likes = doc.data()['likeCount'];
      feedDocProfile.update({'likeCount': likes + 1});
    });

    likesRef.doc(feed.id).collection('feedLikes').doc(currentUserId).set({});

    //addActivity(currentUserId, tweet, false, null);
  }

  static void unlikePost(String currentUserId, Feed feed) {
    DocumentReference feedDocProfile =
        feedsRef.doc(feed.authorId).collection('userFeeds').doc(feed.id);
    feedDocProfile.get().then((doc) {
      int likes = doc.data()['likeCount'];
      feedDocProfile.update({'likeCount': likes - 1});
    });

    likesRef
        .doc(feed.id)
        .collection('feedLikes')
        .doc(currentUserId)
        .get()
        .then((doc) => doc.reference.delete());
  }

  static Future<bool> isLikePost(String currentUserId, Feed feed) async {
    DocumentSnapshot userDoc = await likesRef
        .doc(feed.id)
        .collection('feedLikes')
        .doc(currentUserId)
        .get();

    return userDoc.exists;
  }

  static void commentOnPost({String currentUserId, Feed post, String comment}) {
    commentsRef.doc(post.id).collection('feedComments').add({
      'content': comment,
      'authorId': currentUserId,
      'timestamp': Timestamp.fromDate(DateTime.now()),
    });
    //addActivityItem(currentUserId: currentUserId, post: post, comment: comment);
  }
}
