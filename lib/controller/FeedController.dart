import 'package:combi_mobile/controller/NotificationController.dart';
import 'package:combi_mobile/models/Comment.dart';
import 'package:combi_mobile/models/Feed.dart';
import 'package:combi_mobile/services/StorageService.dart';
import 'package:combi_mobile/utilities/constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class FeedController {
  static final _fireStore = FirebaseFirestore.instance;

  static void createPost(Feed feed) {
    feedsRef.doc(feed.authorId).collection('userFeeds').add({
      'imageUrl': feed.imageUrl,
      'caption': feed.caption,
      'likeCount': feed.likeCount,
      'commentCount': feed.commentCount,
      'authorId': feed.authorId,
      'timestamp': feed.timestamp,
    });
  }

  static Future<List> getUserFeeds(String userId) async {
    QuerySnapshot userFeedsSnap = await feedsRef
        .doc(userId)
        .collection('userFeeds')
        .orderBy('timestamp', descending: true)
        .get();
    List<Feed> userTweets =
        userFeedsSnap.docs.map((doc) => Feed.fromDoc(doc)).toList();

    return userTweets;
  }

  static Future<List> getAllFeeds() async {
    QuerySnapshot userFeedsSnap = await _fireStore
        .collectionGroup('userFeeds')
        .orderBy('timestamp', descending: true)
        .get();
    List<Feed> userTweets =
        userFeedsSnap.docs.map((doc) => Feed.fromDoc(doc)).toList();

    return userTweets;
  }

  static Future<Feed> getFeedById(String userID, String feedID) async {
    DocumentSnapshot feedDocSnapshot =
        await feedsRef.doc(userID).collection('userFeeds').doc(feedID).get();
    if (feedDocSnapshot.exists) {
      return Feed.fromDoc(feedDocSnapshot);
    }
    return Feed();
  }

  static Stream<List<Feed>> getUserFeeds1(String userId) {
    return feedsRef
        .doc(userId)
        .collection('userFeeds')
        .orderBy('timestamp', descending: true)
        .snapshots()
        .map((snapshot) =>
            snapshot.docs.map((doc) => Feed.fromDoc(doc)).toList());
  }

  static Stream<List<Feed>> getAllFeeds1() {
    return _fireStore
        .collectionGroup('userFeeds')
        .orderBy('timestamp', descending: true)
        .snapshots()
        .map((snapshot) =>
            snapshot.docs.map((doc) => Feed.fromDoc(doc)).toList());
  }

  static void deletePost({Feed feed}) async {
    // likesRef.doc(feed.id).get().then((doc) => doc.reference.delete());

    // commentsRef.doc(feed.id).get().then((doc) => doc.reference.delete());
    Future<QuerySnapshot> likes =
        likesRef.doc(feed.id).collection('feedLikes').get();
    likes.then((value) {
      value.docs.forEach((element) {
        likesRef
            .doc(feed.id)
            .collection('feedLikes')
            .doc(element.id)
            .delete()
            .then((value) => print("success"));
      });
    });

    Future<QuerySnapshot> comments =
        commentsRef.doc(feed.id).collection('feedComments').get();
    comments.then((value) {
      value.docs.forEach((element) {
        commentsRef
            .doc(feed.id)
            .collection('feedComments')
            .doc(element.id)
            .delete()
            .then((value) => print("success"));
      });
    });

    feedsRef
        .doc(feed.authorId)
        .collection('userFeeds')
        .doc(feed.id)
        .get()
        .then((doc) => doc.reference.delete());

    StorageService.deleteImage(feed.imageUrl);
    //addActivityItem(currentUserId: currentUserId, post: post, comment: comment);
  }

// void deleteCollection(CollectionReference collection, int batchSize) {
//   try {
//     // retrieve a small batch of documents to avoid out-of-memory errors
//     Future<QuerySnapshot> future = collection.limit(batchSize).get();
//     int deleted = 0;
//     // future.get() blocks on document retrieval
//     List<QueryDocumentSnapshot> documents = future.;
//     for (QueryDocumentSnapshot document : documents) {
//       document.get().delete();
//       ++deleted;
//     }
//     if (deleted >= batchSize) {
//       // retrieve and delete another batch
//       deleteCollection(collection, batchSize);
//     }
//   } catch (Exception e) {
//     System.err.println("Error deleting collection : " + e.getMessage());
//   }
// }
  static void likePost(String currentUserId, Feed feed) {
    DocumentReference feedDocProfile =
        feedsRef.doc(feed.authorId).collection('userFeeds').doc(feed.id);
    feedDocProfile.get().then((doc) {
      int likes = doc.data()['likeCount'];
      feedDocProfile.update({'likeCount': likes + 1});
    });

    likesRef.doc(feed.id).collection('feedLikes').doc(currentUserId).set({});

    if (currentUserId != feed.authorId) {
      NotificationController.addNotification(currentUserId, feed, 'like');
    }
  }

  static void unlikePost(String currentUserId, Feed feed) {
    DocumentReference feedDocProfile =
        feedsRef.doc(feed.authorId).collection('userFeeds').doc(feed.id);
    feedDocProfile.get().then((doc) {
      int likes = doc.data()['likeCount'];
      feedDocProfile.update({'likeCount': likes - 1});
    });

    likesRef
        .doc(feed.id)
        .collection('feedLikes')
        .doc(currentUserId)
        .get()
        .then((doc) => doc.reference.delete());
  }

  static Future<bool> isLikePost(String currentUserId, Feed feed) async {
    DocumentSnapshot userDoc = await likesRef
        .doc(feed.id)
        .collection('feedLikes')
        .doc(currentUserId)
        .get();

    return userDoc.exists;
  }

  static void commentOnPost({String currentUserId, Feed feed, String comment}) {
    DocumentReference feedDocProfile =
        feedsRef.doc(feed.authorId).collection('userFeeds').doc(feed.id);
    feedDocProfile.get().then((doc) {
      int comments = doc.data()['commentCount'];
      feedDocProfile.update({'commentCount': comments + 1});
    });

    commentsRef.doc(feed.id).collection('feedComments').add({
      'content': comment,
      'authorId': currentUserId,
      'timestamp': Timestamp.fromDate(DateTime.now()),
    });
    //addActivityItem(currentUserId: currentUserId, post: post, comment: comment);
    if (currentUserId != feed.authorId) {
      NotificationController.addNotification(currentUserId, feed, 'comment');
    }
  }

  static Future<int> commentsCount(
      {String currentUserId, Feed post, String comment}) async {
    List list = await commentsRef
        .doc(post.id)
        .collection('feedComments')
        .get()
        .then((val) => val.docs);

    return list.length;
    //addActivityItem(currentUserId: currentUserId, post: post, comment: comment);
  }

  static void deleteComment({Feed feed, Comment comment}) async {
    DocumentReference feedDocProfile =
        feedsRef.doc(feed.authorId).collection('userFeeds').doc(feed.id);
    feedDocProfile.get().then((doc) {
      int comments = doc.data()['commentCount'];
      feedDocProfile.update({'commentCount': comments - 1});
    });

    commentsRef
        .doc(feed.id)
        .collection('feedComments')
        .doc(comment.id)
        .get()
        .then((doc) => doc.reference.delete());
    //addActivityItem(currentUserId: currentUserId, post: post, comment: comment);
  }
}
