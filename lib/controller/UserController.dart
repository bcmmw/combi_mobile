import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:combi_mobile/models/Member.dart';
import 'package:combi_mobile/services/auth_service.dart';
import 'package:combi_mobile/services/levelExp_service.dart';
import 'package:combi_mobile/utilities/constants.dart';

class UserController {
  static Future<bool> signUp(Member user) {
    return AuthService.signUp(
      Member(
          username: user.username,
          name: user.name,
          email: user.email,
          password: user.password,
          hpNum: user.hpNum,
          address: user.address),
    );
  }

  static Future<bool> login(String email, String password) {
    return AuthService.login(
      email,
      password,
    );
  }

  // static void updateUser(String userId, String username, String name,
  //     String image, String hpNum, String address) {
  //   usersRef.doc(userId).update({
  //     'username': username,
  //     'name': name,
  //     'image': image,
  //     'hpNum': hpNum,
  //     'address': address,
  //   });
  // }

  static void updateUser(Member user) {
    usersRef.doc(user.id).update({
      'username': user.username,
      'name': user.name,
      'image': user.image,
      'hpNum': user.hpNum,
      'address': user.address,
    });
  }

  static Future<void> updateEmail(String userId, String email) async {
    await usersRef.doc(userId).update({
      'email': email,
    });
  }

  static void levelUp(int pointGained, String currentUserId) {
    DocumentReference user = usersRef.doc(currentUserId);
    user.get().then((doc) {
      int point = doc.data()['point'];
      user.update({
        'point': point + pointGained,
        'level': LevelExpService.calculateLevel(point + pointGained)
      });

      if (!doc.data()['teamID'].isEmpty) {
        DocumentReference team = teamsRef.doc(doc.data()['teamID']);
        team.get().then((doc) {
          int point = doc.data()['point'];
          team.update({
            'point': point + pointGained,
            'level': LevelExpService.calculateLevel(point + pointGained)
          });
        });
      }
    });
  }

  static Future<Member> getUserWithId(String userId) async {
    DocumentSnapshot userDocSnapshot = await usersRef.doc(userId).get();
    if (userDocSnapshot.exists) {
      return Member.fromDoc(userDocSnapshot);
    }
    return Member();
  }

  static Future<Member> getMemberWithId(String userId) async {
    DocumentSnapshot userDocSnapshot = await usersRef.doc(userId).get();
    if (userDocSnapshot.exists) {
      return Member.fromDoc(userDocSnapshot);
    }
    return Member();
  }

  static Stream<List<Member>> getRanking() {
    return usersRef.orderBy('point', descending: true).snapshots().map(
        (snapshot) => snapshot.docs.map((doc) => Member.fromDoc(doc)).toList());
  }

  static Stream<Member> getUserStreamWithId(String userId) {
    return usersRef
        .doc(userId)
        .snapshots()
        .map((snapshot) => Member.fromDoc(snapshot));
  }
}
