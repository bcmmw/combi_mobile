import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:combi_mobile/models/Activity.dart';
import 'package:combi_mobile/utilities/constants.dart';

class ActivityController {
  static void createActivity(Activity activity) {
    activitiesRef.doc(activity.memberId).collection('userActivities').add({
      'beforeImageUrl': activity.beforeImageUrl,
      'afterImageUrl': activity.afterImageUrl,
      'timestamp': activity.timestamp,
      'duration': activity.duration,
      'memberId': activity.memberId,
    });
  }

  static Future<List> getUserActivities(String userId) async {
    QuerySnapshot userActivitiesSnap = await activitiesRef
        .doc(userId)
        .collection('userActivities')
        .orderBy('timestamp', descending: true)
        .get();
    List<Activity> userActivities =
        userActivitiesSnap.docs.map((doc) => Activity.fromDoc(doc)).toList();

    return userActivities;
  }
}
