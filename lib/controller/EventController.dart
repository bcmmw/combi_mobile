import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:combi_mobile/controller/UserController.dart';
import 'package:combi_mobile/models/Event.dart';
import 'package:combi_mobile/models/Member.dart';
import 'package:combi_mobile/utilities/constants.dart';

class EventController {
  static final _fireStore = FirebaseFirestore.instance;

  // static Future<CombiTeam> getUserTeamId(String userId) async {
  //   Future <UserModel> user = UserController.getUserWithId(userId);
  //   DocumentSnapshot teamDocSnapshot =
  //       await teamsRef.doc(user.teamId).get();
  //   if (teamDocSnapshot.exists) {
  //     return CombiTeam.fromDoc(teamDocSnapshot);
  //   }
  //   return CombiTeam();
  // }

  // static Future<List> getRanking() async {
  //   QuerySnapshot userFeedsSnap =
  //       await usersRef.orderBy('point', descending: true).get();
  //   List<UserModel> ranking =
  //       userFeedsSnap.docs.map((doc) => UserModel.fromDoc(doc)).toList();

  //   return ranking;
  // }

  // static Stream<List<Event>> getAllEvents() {
  //   return eventsRef.snapshots().map(
  //       (snapshot) => snapshot.docs.map((doc) => Event.fromDoc(doc)).toList());
  // }

  // static Stream<Event> getEventWithId(String eventId) {
  //   return eventsRef.doc(eventId).snapshots().map((doc) => Event.fromDoc(doc));
  // }

  // static Future<List<CombiTeam>> getRanking() async {
  //   QuerySnapshot teamRankingSnap =
  //       await teamsRef.orderBy('point', descending: true).get();

  //   List<CombiTeam> teamRanking =
  //       teamRankingSnap.docs.map((doc) => CombiTeam.fromDoc(doc)).toList();

  //   return teamRanking;
  // }

  static void joinEvent(String teamId, String eventId, String userId) {
    eventsRef
        .doc(teamId)
        .collection('teamEvents')
        .doc(eventId)
        .collection('eventParticipants')
        .doc(userId)
        .set({});
  }

  static Future<List> getAllEvents() async {
    // return teamsRef.orderBy('point', descending: true).snapshots().map(
    //     (snapshot) =>
    //         snapshot.docs.map((doc) => CombiTeam.fromDoc(doc)).toList());
    QuerySnapshot allEventsSnap = await eventsRef.get();
    List<Event> combiTeams =
        allEventsSnap.docs.map((doc) => Event.fromDoc(doc)).toList();

    return combiTeams;
  }

  static Stream<List> getTeamEvents(String teamId, int index) {
    switch (index) {
      case 0:
        return eventsRef
            .doc(teamId)
            .collection('teamEvents')
            .where('startDateTime', isGreaterThan: DateTime.now())
            .orderBy('startDateTime', descending: true)
            .snapshots()
            .map((snapshot) =>
                snapshot.docs.map((doc) => Event.fromDoc(doc)).toList());
        break;
      case 1:
        return eventsRef
            .doc(teamId)
            .collection('teamEvents')
            .where('startDateTime', isLessThan: DateTime.now())
            .orderBy('startDateTime', descending: true)
            .snapshots()
            .map((snapshot) => snapshot.docs
                .map((doc) => Event.fromDoc(doc))
                .where((i) => i.endDateTime.toDate().isAfter(DateTime.now()))
                .toList());
        break;
      case 2:
        return eventsRef
            .doc(teamId)
            .collection('teamEvents')
            .where('endDateTime', isLessThan: DateTime.now())
            .orderBy('endDateTime', descending: true)
            .snapshots()
            .map((snapshot) =>
                snapshot.docs.map((doc) => Event.fromDoc(doc)).toList());
        break;
    }
  }

  static Stream<List<Future<Member>>> getParticipantsList(
      String teamId, String eventId) {
    return eventsRef
        .doc(teamId)
        .collection('teamEvents')
        .doc(eventId)
        .collection('eventParticipants')
        .snapshots()
        .map((snapshot) => snapshot.docs
            .map((doc) => UserController.getMemberWithId(doc.id))
            .toList());
  }

  static Stream<List> getParticipantsId(String teamId, String eventId) {
    return eventsRef
        .doc(teamId)
        .collection('teamEvents')
        .doc(eventId)
        .collection('eventParticipants')
        .snapshots()
        .map((snapshot) => snapshot.docs.map((doc) => doc.id).toList());

    // List<String> participantsId =
    //     teamEventsSnap.docs.map((doc) => doc.id).toList();

    // return participantsId;
  }

  // static Future<CombiTeam> getTeamWithId1(String teamId) async {
  //   DocumentSnapshot teamDocSnapshot = await teamsRef.doc(teamId).get();
  //   if (teamDocSnapshot.exists) {
  //     return CombiTeam.fromDoc(teamDocSnapshot);
  //   }
  //   return CombiTeam();
  // }
}
