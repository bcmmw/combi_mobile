import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:combi_mobile/models/Feed.dart';
import 'package:combi_mobile/models/Notification.dart';
import 'package:combi_mobile/utilities/constants.dart';

class NotificationController {
  static Future<List> getNotifications(String userId) async {
    QuerySnapshot userNotificationsSnapshot = await notificationsRef
        .doc(userId)
        .collection('userNotifications')
        .orderBy('timestamp', descending: true)
        .get();

    List<NotificationModel> notifications = userNotificationsSnapshot.docs
        .map((doc) => NotificationModel.fromDoc(doc))
        .toList();

    return notifications;
  }

  static void addNotification(String currentUserId, Feed feed, String type) {
    if (type == 'like') {
      notificationsRef.doc(feed.authorId).collection('userNotifications').add({
        'fromUserId': currentUserId,
        'timestamp': Timestamp.fromDate(DateTime.now()),
        "type": 'like',
        'feedID': feed.id
      });
    } else {
      //comment
      notificationsRef.doc(feed.authorId).collection('userNotifications').add({
        'fromUserId': currentUserId,
        'timestamp': Timestamp.fromDate(DateTime.now()),
        "type": 'comment',
        'feedID': feed.id
      });
    }
  }
}
