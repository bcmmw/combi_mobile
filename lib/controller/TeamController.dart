import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:combi_mobile/controller/UserController.dart';
import 'package:combi_mobile/models/CombiTeam.dart';
import 'package:combi_mobile/models/Member.dart';
import 'package:combi_mobile/utilities/constants.dart';
import 'package:combi_mobile/services/levelExp_service.dart';

class TeamController {
  static final _fireStore = FirebaseFirestore.instance;

  static void levelUp(int pointGained, String currentTeamId) {
    DocumentReference team = teamsRef.doc(currentTeamId);
    team.get().then((doc) {
      int point = doc.data()['point'];
      team.update({
        'point': point + pointGained,
        'level': LevelExpService.calculateLevel(point + pointGained)
      });
    });
  }

  // static Future<CombiTeam> getUserTeamId(String userId) async {
  //   Future <UserModel> user = UserController.getUserWithId(userId);
  //   DocumentSnapshot teamDocSnapshot =
  //       await teamsRef.doc(user.teamId).get();
  //   if (teamDocSnapshot.exists) {
  //     return CombiTeam.fromDoc(teamDocSnapshot);
  //   }
  //   return CombiTeam();
  // }

  // static Future<List> getRanking() async {
  //   QuerySnapshot userFeedsSnap =
  //       await usersRef.orderBy('point', descending: true).get();
  //   List<UserModel> ranking =
  //       userFeedsSnap.docs.map((doc) => UserModel.fromDoc(doc)).toList();

  //   return ranking;
  // }

  static void joinTeam(String teamId, String userId) {
    usersRef.doc(userId).update({
      'teamID': teamId,
    });

    teamsRef.doc(teamId).collection('teamMembers').doc(userId).set({});
  }

  static Stream<List<CombiTeam>> getAllTeams() {
    return teamsRef.snapshots().map((snapshot) =>
        snapshot.docs.map((doc) => CombiTeam.fromDoc(doc)).toList());
  }

  static Stream<CombiTeam> getTeamWithId(String teamId) {
    return teamsRef
        .doc(teamId)
        .snapshots()
        .map((doc) => CombiTeam.fromDoc(doc));
  }

  static Future<List<CombiTeam>> getRanking() async {
    QuerySnapshot teamRankingSnap =
        await teamsRef.orderBy('point', descending: true).get();

    List<CombiTeam> teamRanking =
        teamRankingSnap.docs.map((doc) => CombiTeam.fromDoc(doc)).toList();

    return teamRanking;
  }

  static Future<List> getAllTeams1() async {
    // return teamsRef.orderBy('point', descending: true).snapshots().map(
    //     (snapshot) =>
    //         snapshot.docs.map((doc) => CombiTeam.fromDoc(doc)).toList());
    QuerySnapshot combiTeamSnap = await teamsRef.get();
    List<CombiTeam> combiTeams =
        combiTeamSnap.docs.map((doc) => CombiTeam.fromDoc(doc)).toList();

    return combiTeams;
  }

  static Future<CombiTeam> getTeamWithId1(String teamId) async {
    DocumentSnapshot teamDocSnapshot = await teamsRef.doc(teamId).get();
    if (teamDocSnapshot.exists) {
      return CombiTeam.fromDoc(teamDocSnapshot);
    }
    return CombiTeam();
  }

  static Future<List<Member>> getTeamMembers(String teamId) async {
    // DocumentSnapshot teamDocSnapshot = await teamsRef.doc(teamId).collection('teamMembers').get();
    // if (teamDocSnapshot.exists) {
    //   return CombiTeam.fromDoc(teamDocSnapshot);
    // }
    // return CombiTeam();

    QuerySnapshot combiTeamSnap =
        await teamsRef.doc(teamId).collection('teamMembers').get();

    List<Member> teamMembers = combiTeamSnap.docs.map((doc) {
      UserController.getUserWithId(doc.id);
    }).toList();

    return teamMembers;
  }
}
