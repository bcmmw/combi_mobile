import 'package:combi_mobile/config/palette.dart';
import 'package:combi_mobile/screens/screens.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Widget getScreenId() {
    return StreamBuilder(
        stream: FirebaseAuth.instance.authStateChanges(),
        builder: (BuildContext context, snapshot) {
          if (snapshot.hasData) {
            //return FeedScreen();
            return HomeScreen(currentUserId: snapshot.data.uid);
          } else {
            return SplashScreen();
          }
        });
  }

  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'COMBI App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: Palette.primaryColor,
      ),
      home: getScreenId(),
    );
  }
}
