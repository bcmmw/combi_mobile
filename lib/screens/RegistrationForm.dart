import 'package:combi_mobile/controller/UserController.dart';
import 'package:combi_mobile/models/Member.dart';
import 'package:combi_mobile/services/auth_service.dart';
import 'package:combi_mobile/widgets/RoundedButton.dart';
import 'package:combi_mobile/widgets/widgets.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class RegistrationForm extends StatefulWidget {
  RegistrationForm({Key key}) : super(key: key);

  @override
  _RegistrationFormState createState() => _RegistrationFormState();
}

class _RegistrationFormState extends State<RegistrationForm> {
  final _formKey = new GlobalKey<FormState>();

  final _cName = TextEditingController();
  final _cUserName = TextEditingController();
  final _cAddress = TextEditingController();
  final _cEmail = TextEditingController();
  final _cHp = TextEditingController();
  dynamic _cPassword = TextEditingController();
  dynamic _cConfirmPassword = TextEditingController();

  bool _obscureText = true;
  bool _obscureText1 = true;
  String msg;

  @override
  Widget build(BuildContext context) {
    //Elements of Registration Form
    TextFormField inputUserName = TextFormField(
      controller: _cUserName,
      inputFormatters: [
        LengthLimitingTextInputFormatter(45),
      ],
      keyboardType: TextInputType.text,
      decoration: inputDecoration('Username', Icons.person),
      validator: (value) {
        if (value.isEmpty) {
          return 'Required';
        }
        return null;
      },
    );

    TextFormField inputName = TextFormField(
      controller: _cName,
      inputFormatters: [
        LengthLimitingTextInputFormatter(25),
      ],
      keyboardType: TextInputType.text,
      decoration:
          inputDecoration('Full Name (Same as IC)', Icons.account_box_outlined),
      validator: (value) {
        if (value.isEmpty) {
          return 'Required';
        }
        return null;
      },
    );

    TextFormField inputAddress = TextFormField(
      controller: _cAddress,
      inputFormatters: [
        LengthLimitingTextInputFormatter(1000),
      ],
      keyboardType: TextInputType.text,
      decoration: inputDecoration('Address', Icons.home),
      validator: (value) {
        if (value.isEmpty) {
          return 'Required';
        }
        return null;
      },
    );

    TextFormField inputEmail = TextFormField(
      controller: _cEmail,
      inputFormatters: [
        LengthLimitingTextInputFormatter(50),
      ],
      keyboardType: TextInputType.emailAddress,
      decoration: inputDecoration('Email', Icons.email),
      validator: (value) {
        if (value.isEmpty) {
          return 'Required';
        } else if (!EmailValidator.validate(value)) {
          return 'Wrong email format, ex: xyz@gmail.com';
        }
        return null;
      },
    );

    TextFormField inputPassword = TextFormField(
      controller: _cPassword,
      inputFormatters: [
        LengthLimitingTextInputFormatter(50),
      ],
      keyboardType: TextInputType.text,
      obscureText: _obscureText,
      decoration: inputDecoration('Password', Icons.lock,
          suffixIcon: InkWell(
            onTap: _toggle,
            child: Icon(
              _obscureText ? FontAwesomeIcons.eyeSlash : FontAwesomeIcons.eye,
              size: 15.0,
              color: Colors.black,
            ),
          )),
      validator: (value) {
        if (value.isEmpty) {
          return 'Required';
        } else if (value.length < 6) {
          return 'Password too short. Must more than 6 characters';
        }
        return null;
      },
    );

    TextFormField inputConfirmPassword = TextFormField(
      controller: _cConfirmPassword,
      inputFormatters: [
        LengthLimitingTextInputFormatter(50),
      ],
      keyboardType: TextInputType.text,
      obscureText: _obscureText1,
      decoration: inputDecoration('Confirm Password', Icons.lock,
          suffixIcon: InkWell(
            onTap: _toggle1,
            child: Icon(
              _obscureText1 ? FontAwesomeIcons.eyeSlash : FontAwesomeIcons.eye,
              size: 15.0,
              color: Colors.black,
            ),
          )),
      validator: (value) {
        if (value.isEmpty) {
          return 'Required';
        } else if (value.isNotEmpty) {
          if (value != _cPassword.text) {
            return 'Password does not match';
          }
        }
        return null;
      },
    );

    TextFormField inputHp = TextFormField(
      controller: _cHp,
      maxLength: 11,
      validator: (value) {
        return value.length < 10 ? 'Please enter valid hp number' : null;
      },
      keyboardType: TextInputType.phone,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.digitsOnly
      ],
      decoration: inputDecoration('Hp Number', Icons.phone),
    );

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
      child: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Login Info",
                style: GoogleFonts.openSans(
                  textStyle: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
              inputEmail,
              SizedBox(height: 20),
              Text(
                "Password",
                style: GoogleFonts.openSans(
                  textStyle: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
              inputPassword,
              inputConfirmPassword,
              SizedBox(height: 25),
              Text(
                "User Info",
                style: GoogleFonts.openSans(
                  textStyle: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
              inputUserName,
              inputName,
              inputHp,
              inputAddress,
              Text(""),
              Text(""),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: Text(
                      "By signing up, you agree to all the terms and conditions",
                      style: GoogleFonts.openSans(
                          textStyle: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w700,
                        color: Colors.black54,
                      )),
                    ),
                  ),
                  SizedBox(height: 50),
                  RoundedButton(
                    btnText: 'SIGN UP',
                    onBtnPressed: () async {
                      if (_formKey.currentState.validate()) {
                        bool isValid = await UserController.signUp(
                          Member(
                              username: _cUserName.text,
                              name: _cName.text,
                              email: _cEmail.text,
                              password: _cPassword.text,
                              hpNum: _cHp.text,
                              address: _cAddress.text),
                        );
                        if (isValid) {
                          Navigator.of(context)
                              .popUntil((route) => route.isFirst);
                        } else {
                          snackBar(
                              context, AuthService.errorMsg, Colors.redAccent);
                        }
                      }
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _toggle() {
    setState(() => _obscureText = !_obscureText);
  }

  void _toggle1() {
    setState(() => _obscureText1 = !_obscureText1);
  }
}
