import 'package:combi_mobile/controller/ActivityController.dart';
import 'package:combi_mobile/models/Activity.dart';
import 'package:combi_mobile/widgets/Background.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:url_launcher/url_launcher.dart';

class MyActivity extends StatefulWidget {
  final String currentUserId;

  const MyActivity({Key key, this.currentUserId}) : super(key: key);
  @override
  _MyActivityState createState() => _MyActivityState();
}

class _MyActivityState extends State<MyActivity> {
  List<Activity> activity;

  // TextEditingController _result = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            'My Activity',
            style: TextStyle(color: Colors.black),
          ),
        ),
        body: FutureBuilder<List>(
            future: ActivityController.getUserActivities(widget.currentUserId),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                activity = snapshot.data;
                return Background(
                  child: Center(
                    child: Container(
                      alignment: Alignment.topCenter,
                      //margin: EdgeInsets.fromLTRB(5, 30, 5, 0),
                      child: _tableBody(),
                    ),
                  ),
                );
              } else {
                return Background(
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        CircularProgressIndicator(),
                        SizedBox(height: 50),
                        Text('Loading Activity Info... Please wait'),
                      ],
                    ),
                  ),
                );
              }
            }));
  }

  SingleChildScrollView _tableBody() {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: DataTable(
            sortColumnIndex: 0,
            sortAscending: true,
            columns: [
              DataColumn(
                label: Text('Time'),
              ),
              DataColumn(
                label: Text('Duration'),
              ),
              DataColumn(
                label: Text('Before'),
              ),
              DataColumn(
                label: Text('After'),
              ),
              DataColumn(
                label: Text('Points'),
              ),
            ],
            rows: activity
                .map((c) => DataRow(cells: [
                      DataCell(Center(
                        child: Text(
                          c.timestamp.toDate().toString().substring(0, 19),
                        ),
                      )),
                      DataCell(Center(
                        child: Text(
                          c.duration.toString(),
                        ),
                      )),
                      DataCell(
                        Center(
                          child: GestureDetector(
                            onTap: () async {
                              var url = c.beforeImageUrl;
                              if (await canLaunch(url)) {
                                await launch(url);
                              } else {
                                throw 'Could not launch $url';
                              }
                            },
                            child: Text(
                              'Before',
                              style: GoogleFonts.openSans(
                                textStyle: TextStyle(
                                  decoration: TextDecoration.underline,
                                  color: Colors.blueAccent,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      DataCell(Center(
                        child: GestureDetector(
                          onTap: () async {
                            var url = c.afterImageUrl;
                            if (await canLaunch(url)) {
                              await launch(url);
                            } else {
                              throw 'Could not launch $url';
                            }
                          },
                          child: Text(
                            'After',
                            style: GoogleFonts.openSans(
                              textStyle: TextStyle(
                                decoration: TextDecoration.underline,
                                color: Colors.blueAccent,
                                fontSize: 12,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                        ),
                        //Text(c.afterImageUrl)
                      )),
                      DataCell(Center(child: Text('50'))),
                    ]))
                .toList()),
      ),
    );
  }
}
