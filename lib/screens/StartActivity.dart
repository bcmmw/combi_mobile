import 'package:combi_mobile/config/palette.dart';
import 'package:combi_mobile/screens/ActivityPage.dart';
import 'package:combi_mobile/widgets/widgets.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class StartActivity extends StatefulWidget {
  final String currentUserId;
  StartActivity({Key key, this.currentUserId}) : super(key: key);

  @override
  _StartActivityState createState() => _StartActivityState();
}

class _StartActivityState extends State<StartActivity> {
  PageController _pageController = new PageController();
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: null,
        child: Scaffold(
          backgroundColor: const Color(0xffffffff),
          body: Stack(
            children: [
              Background(),
              PageView(
                controller: _pageController,
                children: [
                  _tips(
                      'DENGUE PREVENTION',
                      'We can prevent dengue by spending at least 10 minutes over the weekend looking for and destroying Aedes breeding grounds',
                      'OK'),
                  _tips(
                      'Only 10 Minutes',
                      'In this 10 minutes, find and destroy any potential Aedes breeding sites in your area and take picture BEFORE & AFTER as proof to level up and get more rewards!',
                      'START NOW'),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.all(15),
                    child: Center(
                      child: SmoothPageIndicator(
                        controller: _pageController,
                        count: 2,
                        effect: SlideEffect(
                            spacing: 10.0,
                            radius: 100,
                            dotWidth: 15.0,
                            dotHeight: 15.0,
                            paintStyle: PaintingStyle.fill,
                            strokeWidth: 1.5,
                            dotColor: Colors.black45,
                            activeDotColor: Palette.primaryColor),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ));
  }

  Padding _tips(String title, String content, String button) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20.0),
      child: Expanded(
        child: Align(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  _buildText(title, 28, Color(0xff000000), 0.7142857142857143),
                ],
              ),
              SizedBox(height: 75),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: 200.0,
                    height: 250.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(28.0),
                      color: Palette.primaryColor,
                      boxShadow: [
                        BoxShadow(
                          color: const Color(0x29000000),
                          offset: Offset(5, 9),
                          blurRadius: 5,
                        ),
                      ],
                    ),
                    child: Center(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: _buildText(
                            content, 18, Color(0xb2000000), 1.1111111111111112),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 50),
              RoundedButton(
                  color: Colors.black87,
                  btnText: button,
                  onBtnPressed: () {
                    _pageController.page == 0
                        ? _pageController.animateToPage(1,
                            curve: Curves.ease, duration: Duration(seconds: 1))
                        :
                        //print('Start Activity');
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => ActivityPage(
                                  currentUserId: widget.currentUserId,
                                )));
                  }),
              // InkWell(
              //   onTap: () {
              //     _pageController.page == 0
              //         ? _pageController.animateToPage(1,
              //             curve: Curves.ease, duration: Duration(seconds: 1))
              //         : Navigator.of(context).push(MaterialPageRoute(
              //             builder: (context) => WelcomeScreen()));
              //   },
              //   child: Container(
              //     width: 100.0,
              //     height: 35.0,
              //     decoration: BoxDecoration(
              //       borderRadius: BorderRadius.circular(12.0),
              //       color: Palette.primaryColor,
              //       boxShadow: [
              //         BoxShadow(
              //           color: const Color(0x29000000),
              //           offset: Offset(0, 0),
              //           blurRadius: 99,
              //         ),
              //       ],
              //     ),
              // child:
              //  SizedBox(
              //   width: 82.0,
              //   height: 17.0,
              //   child: _buildText(button, 15, Color(0xff000000), null),
              // ),
            ],
          ),
        ),
      ),
    );
  }

  Text _buildText(String text, double size, Color color, double height) {
    return Text(
      text,
      style: GoogleFonts.robotoSlab(
        textStyle: TextStyle(
          fontSize: size,
          color: const Color(0xff000000),
          fontWeight: FontWeight.w700,
          height: height,
        ),
      ),
      textHeightBehavior: TextHeightBehavior(applyHeightToFirstAscent: true),
      textAlign: TextAlign.center,
    );
  }
}
