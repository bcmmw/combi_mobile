import 'package:combi_mobile/models/Feed.dart';
import 'package:combi_mobile/models/Member.dart';
import 'package:combi_mobile/screens/MyActivityScreen.dart';
import 'package:combi_mobile/screens/screens.dart';
import 'package:combi_mobile/services/auth_service.dart';
import 'package:combi_mobile/services/levelExp_service.dart';
import 'package:combi_mobile/utilities/constants.dart';
import 'package:combi_mobile/utilities/styling.dart';
import 'package:combi_mobile/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'dart:ui' as ui;

class ProfileScreen extends StatefulWidget {
  final String currentUserId;
  final String visitedUserId;
  final bool fromOtherPage;

  const ProfileScreen(
      {Key key,
      this.currentUserId,
      this.visitedUserId,
      this.fromOtherPage = false})
      : super(key: key);
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  int _followersCount = 0;
  int _followingCount = 0;
  bool _isFollowing = false;
  int _profileSegmentedValue = 0;
  double _currentSliderValue = 0;

  List<Feed> _feeds = [];

  Widget buildProfileWidgets(Member author) {
    return FeedStreamBuilder(
      currentUserId: widget.currentUserId,
      allFeed: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: widget.fromOtherPage
            ? AppBar(
                backgroundColor: Colors.white,
                centerTitle: true,
                title: widget.currentUserId != widget.visitedUserId
                    ? Text(
                        'Profile',
                        style: TextStyle(color: Colors.black),
                      )
                    : Text(
                        'Me',
                        style: TextStyle(color: Colors.black),
                      ),
              )
            : null,
        backgroundColor: Colors.white,
        body: Background(
          child: FutureBuilder(
            future: usersRef.doc(widget.visitedUserId).get(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (!snapshot.hasData) {
                return Center(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(kPrimaryColor),
                  ),
                );
              }
              Member userModel = Member.fromDoc(snapshot.data);
              _currentSliderValue = userModel.point.toDouble();

              return ListView(
                physics: const BouncingScrollPhysics(
                    parent: AlwaysScrollableScrollPhysics()),
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 25),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox.shrink(),
                        widget.currentUserId == widget.visitedUserId
                            ? PopupMenuButton(
                                icon: Icon(
                                  Icons.more_horiz,
                                  color: Colors.black45,
                                  size: 30,
                                ),
                                itemBuilder: (_) {
                                  return <PopupMenuItem<String>>[
                                    new PopupMenuItem(
                                      child: Text('Logout'),
                                      value: 'logout',
                                    )
                                  ];
                                },
                                onSelected: (selectedItem) {
                                  if (selectedItem == 'logout') {
                                    AuthService.logout();
                                    Navigator.of(context)
                                        .popUntil((route) => route.isFirst);
                                  }
                                },
                              )
                            : SizedBox(),
                      ],
                    ),
                  ),
                  Container(
                    transform: Matrix4.translationValues(0.0, -40.0, 0.0),
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            CircleAvatar(
                              radius: 45,
                              backgroundImage: userModel.image.isEmpty
                                  ? AssetImage('assets/images/placeholder.png')
                                  : NetworkImage(userModel.image),
                            ),
                            SizedBox(width: 15.0),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 10),
                                Text(
                                  userModel.name,
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(height: 10),
                                Text(
                                  '@' + userModel.username,
                                  style: TextStyle(
                                    fontSize: 15,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(height: 15),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              ' Level ' + userModel.level.toString(),
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                letterSpacing: 2,
                              ),
                            ),
                            Text(
                              userModel.point.toString() +
                                  '/' +
                                  LevelExpService.calculateLevelExp(
                                          userModel.level)
                                      .toString(),
                              style: TextStyle(
                                fontSize: 13,
                              ),
                            ),
                          ],
                        ),
                        Slider(
                          value: _currentSliderValue,
                          min: 0,
                          max:
                              LevelExpService.calculateLevelExp(userModel.level)
                                  .toDouble(),
                          label: _currentSliderValue.toString(),
                          activeColor: kPrimaryColor,
                          inactiveColor: Colors.amber[100],
                          onChanged: (double value) {
                            setState(() {
                              _currentSliderValue = value;
                            });
                          },
                        ),
                        SizedBox(height: 10),
                      ],
                    ),
                  ),
                  widget.currentUserId == widget.visitedUserId
                      ? ClipRect(
                          child: BackdropFilter(
                            filter:
                                ui.ImageFilter.blur(sigmaX: 15.0, sigmaY: 15.0),
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.transparent,
                                borderRadius: BorderRadius.circular(20),
                                border: Border.all(color: Colors.black12),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        GestureDetector(
                                          onTap: () async {
                                            await Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    EditProfileScreen(
                                                  user: userModel,
                                                ),
                                              ),
                                            );
                                          },
                                          child: Column(
                                            children: [
                                              IconButton(
                                                icon: Icon(Icons.edit),
                                                iconSize: 30.0,
                                                onPressed: () async {
                                                  await Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          EditProfileScreen(
                                                        user: userModel,
                                                      ),
                                                    ),
                                                  );
                                                },
                                              ),
                                              Text(
                                                'Edit Profile',
                                                style: TextStyle(
                                                  fontSize: 15.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        GestureDetector(
                                          onTap: () async {
                                            await Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    MyActivity(
                                                  currentUserId: userModel.id,
                                                ),
                                              ),
                                            );
                                          },
                                          child: Column(
                                            children: [
                                              IconButton(
                                                icon: Icon(Icons.history),
                                                iconSize: 30.0,
                                                onPressed: () async {
                                                  await Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          MyActivity(
                                                        currentUserId:
                                                            userModel.id,
                                                      ),
                                                    ),
                                                  );
                                                }, //_toCommentScreen,
                                              ),
                                              Text(
                                                'My Activity',
                                                style: TextStyle(
                                                  fontSize: 15.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        GestureDetector(
                                          onTap: () async {
                                            await Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    RankingScreen(
                                                  currentUserId: userModel.id,
                                                ),
                                              ),
                                            );
                                          },
                                          child: Column(
                                            children: [
                                              IconButton(
                                                icon: Icon(
                                                    Icons.analytics_outlined),
                                                iconSize: 30.0,
                                                onPressed: () async {
                                                  await Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          RankingScreen(
                                                        currentUserId:
                                                            userModel.id,
                                                      ),
                                                    ),
                                                  );
                                                }, //_toCommentScreen,
                                              ),
                                              Text(
                                                'Ranking',
                                                style: TextStyle(
                                                  fontSize: 15.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        )
                      : SizedBox.shrink(),
                  SizedBox(height: 25),
                  FeedStreamBuilder(
                    currentUserId: widget.currentUserId,
                    visitedUserId: widget.visitedUserId,
                    allFeed: false,
                  ),
                ],
              );
            },
          ),
        ));
  }
}
