import 'package:combi_mobile/controller/NotificationController.dart';
import 'package:combi_mobile/models/Notification.dart';
import 'package:combi_mobile/models/Member.dart';
import 'package:combi_mobile/screens/ProfileScreen.dart';
import 'package:combi_mobile/screens/ViewPostScreen.dart';
import 'package:combi_mobile/utilities/constants.dart';
import 'package:combi_mobile/widgets/Background.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class NotificationScreen extends StatefulWidget {
  final String currentUserId;

  const NotificationScreen({Key key, this.currentUserId}) : super(key: key);
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  List<NotificationModel> _notifications = [];

  setupActivities() async {
    List<NotificationModel> notifications =
        await NotificationController.getNotifications(widget.currentUserId);
    if (mounted) {
      setState(() {
        _notifications = notifications;
      });
    }
  }

  buildActivity(NotificationModel notification) {
    return FutureBuilder(
        future: usersRef.doc(notification.fromUserId).get(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (!snapshot.hasData) {
            return SizedBox.shrink();
          } else {
            Member user = Member.fromDoc(snapshot.data);
            return GestureDetector(
              onTap: () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => ViewPostScreen(
                    currentUserId: widget.currentUserId,
                    feedID: notification.feedID,
                  ),
                ),
              ),
              child: Column(
                children: [
                  ListTile(
                    leading: GestureDetector(
                        onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (_) => ProfileScreen(
                                  currentUserId: widget.currentUserId,
                                  visitedUserId: notification.fromUserId,
                                  fromOtherPage: true,
                                ),
                              ),
                            ),
                        child: CircleAvatar(
                          radius: 20,
                          backgroundImage: user.image.isEmpty
                              ? AssetImage('assets/images/placeholder.png')
                              : NetworkImage(user.image),
                        )),
                    title: notification.type == 'like'
                        ? Text('${user.name} liked your post')
                        : Text('${user.name} commented your post'),
                    subtitle: Text(
                      notification.timestamp
                          .toDate()
                          .toString()
                          .substring(0, 19),
                      style: TextStyle(color: Colors.black54),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Divider(
                      //color: kPrimaryColor,
                      thickness: 1,
                    ),
                  )
                ],
              ),
            );
          }
        });
  }

  @override
  void initState() {
    super.initState();
    setupActivities();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            'Notifications',
            style: TextStyle(color: Colors.black),
          ),
        ),
        body: Background(
            child: RefreshIndicator(
          onRefresh: () => setupActivities(),
          child: _notifications.length < 0
              ? Center(
                  child: Text(
                    'You have no notifications.',
                    style: GoogleFonts.robotoSlab(
                      textStyle: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                )
              : ListView.builder(
                  itemCount: _notifications.length,
                  itemBuilder: (BuildContext context, int index) {
                    NotificationModel notification = _notifications[index];
                    return buildActivity(notification);
                  }),
        )));
  }
}
