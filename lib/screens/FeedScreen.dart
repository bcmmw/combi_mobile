import 'package:combi_mobile/controller/FeedController.dart';
import 'package:combi_mobile/models/Feed.dart';
import 'package:combi_mobile/models/Member.dart';
import 'package:combi_mobile/screens/CreatePostScreen.dart';
import 'package:combi_mobile/utilities/constants.dart';
import 'package:combi_mobile/utilities/styling.dart';
import 'package:combi_mobile/widgets/FeedStreamBuilder.dart';
import 'package:combi_mobile/widgets/Background.dart';
import 'package:combi_mobile/widgets/PostView.dart';
import 'package:flutter/material.dart';

class FeedScreen extends StatefulWidget {
  final String currentUserId;

  const FeedScreen({Key key, this.currentUserId}) : super(key: key);

  @override
  _FeedScreenState createState() => _FeedScreenState();
}

class _FeedScreenState extends State<FeedScreen> {
  List _followingTweets = [];
  bool _loading = false;

  buildTweets(Feed feed, Member author) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: PostView(
        feed: feed,
        author: author,
        currentUserId: widget.currentUserId,
      ),
    );
  }

  showFollowingTweets(String currentUserId) {
    List<Widget> followingTweetsList = [];
    for (Feed feed in _followingTweets) {
      followingTweetsList.add(FutureBuilder(
          future: usersRef.doc(feed.authorId).get(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              Member author = Member.fromDoc(snapshot.data);
              return buildTweets(feed, author);
            } else {
              return SizedBox.shrink();
            }
          }));
    }
    return followingTweetsList;
  }

  setupFollowingTweets() async {
    setState(() {
      _loading = true;
    });
    List followingTweets = await FeedController.getAllFeeds();
    if (mounted) {
      setState(() {
        _followingTweets = followingTweets;
        _loading = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    setupFollowingTweets();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      floatingActionButton: FloatingActionButton(
        backgroundColor: kPrimaryColor,
        child: const Icon(Icons.add),
        onPressed: () async {
          //print('create new post');
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CreatePostScreen(
                  currentUserId: widget.currentUserId,
                ),
              ));
        },
      ),
      body: Background(
        child: RefreshIndicator(
          color: kPrimaryColor,
          onRefresh: () => setupFollowingTweets(),
          child: FeedStreamBuilder(
            currentUserId: widget.currentUserId,
            allFeed: true,
          ),
        ),
      ),
    );
  }
}
