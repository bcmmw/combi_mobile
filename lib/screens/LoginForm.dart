import 'package:combi_mobile/controller/UserController.dart';
import 'package:combi_mobile/services/auth_service.dart';
import 'package:combi_mobile/widgets/widgets.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class LoginForm extends StatefulWidget {
  LoginForm({Key key}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = new GlobalKey<FormState>();
  final _cEmail = TextEditingController();
  dynamic _cPassword = TextEditingController();
  bool _obscureText = true;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
      child: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: new Column(
            children: <Widget>[
              SizedBox(height: 20),
              new TextFormField(
                controller: _cEmail,
                inputFormatters: [
                  LengthLimitingTextInputFormatter(50),
                ],
                keyboardType: TextInputType.emailAddress,
                decoration: inputDecoration('Email', Icons.email),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Required';
                  } else if (!EmailValidator.validate(value)) {
                    return 'Wrong email format, ex: xyz@gmail.com';
                  }
                  return null;
                },
              ),
              SizedBox(height: 20),
              new TextFormField(
                controller: _cPassword,
                inputFormatters: [
                  LengthLimitingTextInputFormatter(50),
                ],
                keyboardType: TextInputType.text,
                obscureText: _obscureText,
                decoration: inputDecoration('Password', Icons.lock,
                    suffixIcon: InkWell(
                      onTap: _toggle,
                      child: Icon(
                        _obscureText
                            ? FontAwesomeIcons.eyeSlash
                            : FontAwesomeIcons.eye,
                        size: 15.0,
                        color: Colors.black,
                      ),
                    )),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Required';
                  } else
                    return null;
                },
              ),
              SizedBox(height: 50),
              RoundedButton(
                btnText: 'LOG IN',
                onBtnPressed: () async {
                  if (_formKey.currentState.validate()) {
                    bool isValid = await UserController.login(
                      _cEmail.text,
                      _cPassword.text,
                    );
                    if (isValid) {
                      Navigator.of(context).popUntil((route) => route.isFirst);
                    } else {
                      snackBar(context, AuthService.errorMsg, Colors.redAccent);
                    }
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _toggle() {
    setState(() => _obscureText = !_obscureText);
  }
}
