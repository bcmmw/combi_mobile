import 'package:combi_mobile/services/auth_service.dart';
import 'package:combi_mobile/utilities/styling.dart';
import 'package:combi_mobile/widgets/AlertDialog.dart';
import 'package:combi_mobile/widgets/widgets.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

class ChangeEmail extends StatefulWidget {
  final UserCredential authResult;

  const ChangeEmail({Key key, this.authResult}) : super(key: key);
  @override
  _ChangeEmailState createState() => _ChangeEmailState();
}

class _ChangeEmailState extends State<ChangeEmail> {
  final _formKey = new GlobalKey<FormState>();
  dynamic _cEmail = TextEditingController();

  void initState() {
    super.initState();
    _cEmail.text = widget.authResult.user.email;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'Change Email or Password',
          style: kBottomNavigationBarStyle,
        ),
      ),
      body: Background(
        child: Container(
          child: Column(
            children: [
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Text(
                  "Enter new email/ password",
                  style: GoogleFonts.openSans(
                    textStyle: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 15),
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        TextFormField(
                          controller: _cEmail,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(50),
                          ],
                          keyboardType: TextInputType.emailAddress,
                          decoration: inputDecoration('Email', Icons.email),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Required';
                            } else if (!EmailValidator.validate(value)) {
                              return 'Wrong email format, ex: xyz@gmail.com';
                            }
                            return null;
                          },
                        ),
                        SizedBox(height: 15),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(height: 15),
              RoundedButton(
                btnText: 'SAVE',
                onBtnPressed: () async {
                  if (_formKey.currentState.validate()) {
                    bool isValid = await AuthService.updateEmail(
                      _cEmail.text,
                    );
                    if (isValid) {
                      alertDialog(
                        context,
                        'Changes saved successfully, please login again.',
                        () {
                          AuthService.logout();
                          Navigator.of(context)
                              .popUntil((route) => route.isFirst);
                        },
                      );
                    } else {
                      snackBar(context, AuthService.errorMsg, Colors.redAccent);
                    }
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
