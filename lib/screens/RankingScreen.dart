import 'package:combi_mobile/controller/UserController.dart';
import 'package:combi_mobile/models/Member.dart';
import 'package:combi_mobile/widgets/Background.dart';
import 'package:combi_mobile/widgets/RankingContainer.dart';
import 'package:flutter/material.dart';

class RankingScreen extends StatefulWidget {
  final String currentUserId;

  RankingScreen({this.currentUserId});

  @override
  _RankingScreenState createState() => _RankingScreenState();
}

class _RankingScreenState extends State<RankingScreen> {
  /// SETUP FEED is unnecessary because we switched it from a FutureBuilder to a StreamBuilder

  // List<Post> _posts = [];

  // @override
  // void initState() {
  //   super.initState();
  //   _setupFeed();
  // }

  // _setupFeed() async {
  //   List<Post> posts = await DatabaseService.getFeedPosts(widget.currentUserId);
  //   setState(() {
  //     _posts = posts;
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          'Ranking',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Background(
        child: StreamBuilder(
          stream: UserController.getRanking(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (!snapshot.hasData) {
              return SizedBox.shrink();
            }
            final List<Member> userRanking = snapshot.data;
            return ListView.builder(
              itemCount: userRanking.length,
              itemBuilder: (BuildContext context, int index) {
                Member user = userRanking[index];
                return FutureBuilder(
                  future: UserController.getUserWithId(user.id),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (!snapshot.hasData) {
                      return SizedBox.shrink();
                    }
                    Member author = snapshot.data;
                    return RankingContainer(
                      currentUserId: widget.currentUserId,
                      user: user,
                      rank: index + 1,
                    );
                  },
                );
              },
            );
          },
        ),
      ),
    );
  }
}
