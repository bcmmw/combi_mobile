import 'dart:io';
import 'package:combi_mobile/models/Member.dart';
import 'package:combi_mobile/screens/Reauthenticate.dart';
import 'package:combi_mobile/services/StorageService.dart';
import 'package:combi_mobile/utilities/styling.dart';
import 'package:combi_mobile/widgets/widgets.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EditProfileScreen extends StatefulWidget {
  final Member user;

  const EditProfileScreen({Key key, this.user}) : super(key: key);
  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  String _name;
  File _profileImage;
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;
  final _cName = TextEditingController();
  final _cUserName = TextEditingController();
  final _cHp = TextEditingController();
  final _cAddress = TextEditingController();
  bool _obscureText = true;
  bool _obscureText1 = true;

  displayProfileImage() {
    if (_profileImage == null) {
      if (widget.user.image.isEmpty) {
        return AssetImage('assets/images/placeholder.png');
      } else {
        return NetworkImage(widget.user.image);
      }
    } else {
      return FileImage(_profileImage);
    }
  }

  saveProfile() async {
    _formKey.currentState.save();
    if (_formKey.currentState.validate() && !_isLoading) {
      setState(() {
        _isLoading = true;
      });
      String profilePictureUrl = '';
      if (_profileImage == null) {
        profilePictureUrl = widget.user.image;
      } else {
        profilePictureUrl = await StorageService.uploadProfilePicture(
            widget.user.image, _profileImage);
      }

      Member user = Member(
        id: widget.user.id,
        username: _cUserName.text,
        name: _cName.text,
        hpNum: _cHp.text,
        address: _cAddress.text,
        image: profilePictureUrl,
      );

      // UserController.updateUser(widget.user.id, _cUserName.text, _cName.text,
      //     profilePictureUrl, _cHp.text, _cAddress.text);

      snackBar(context, 'Changes saved successfully.', Colors.greenAccent);
      Navigator.pop(context);
    }
  }

  handleImageFromGallery() async {
    try {
      final picker = ImagePicker();
      File imageFile;

      final pickedFile = await picker.getImage(source: ImageSource.gallery);

      if (pickedFile != null) {
        imageFile = File(pickedFile.path);
        setState(() {
          _profileImage = imageFile;
        });
      }
    } catch (e) {
      print(e);
    }
  }

  void _toggle() {
    setState(() => _obscureText = !_obscureText);
  }

  void _toggle1() {
    setState(() => _obscureText1 = !_obscureText1);
  }

  @override
  void initState() {
    super.initState();
    _cUserName.text = widget.user.username;
    _cName.text = widget.user.name;
    _cHp.text = widget.user.hpNum;
    _cAddress.text = widget.user.address;
  }

  @override
  Widget build(BuildContext context) {
    //Elements of Registration Form
    TextFormField inputUserName = TextFormField(
      controller: _cUserName,
      inputFormatters: [
        LengthLimitingTextInputFormatter(45),
      ],
      keyboardType: TextInputType.text,
      decoration: inputDecoration('Username', Icons.person),
      validator: (value) {
        if (value.isEmpty) {
          return 'Required';
        }
        return null;
      },
    );

    TextFormField inputName = TextFormField(
      controller: _cName,
      inputFormatters: [
        LengthLimitingTextInputFormatter(25),
      ],
      keyboardType: TextInputType.text,
      decoration:
          inputDecoration('Full Name (Same as IC)', Icons.account_box_outlined),
      validator: (value) {
        if (value.isEmpty) {
          return 'Required';
        }
        return null;
      },
    );

    TextFormField inputHp = TextFormField(
      controller: _cHp,
      maxLength: 11,
      validator: (value) {
        return value.length < 10 ? 'Please enter valid hp number' : null;
      },
      keyboardType: TextInputType.phone,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.digitsOnly
      ],
      decoration: inputDecoration('Hp Number', Icons.phone),
    );

    TextFormField inputAddress = TextFormField(
      controller: _cAddress,
      inputFormatters: [
        LengthLimitingTextInputFormatter(1000),
      ],
      keyboardType: TextInputType.text,
      decoration: inputDecoration('Address', Icons.home),
      validator: (value) {
        if (value.isEmpty) {
          return 'Required';
        }
        return null;
      },
    );

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'Edit Profile',
          style: kBottomNavigationBarStyle,
        ),
      ),
      body: Background(
        child: ListView(
          physics: const BouncingScrollPhysics(
              parent: AlwaysScrollableScrollPhysics()),
          children: [
            Container(
              transform: Matrix4.translationValues(0, 50, 0),
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      GestureDetector(
                        onTap: () {
                          handleImageFromGallery();
                        },
                        child: Stack(
                          children: [
                            CircleAvatar(
                              radius: 45,
                              backgroundImage: displayProfileImage(),
                            ),
                            CircleAvatar(
                              radius: 45,
                              backgroundColor: Colors.black54,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Icon(
                                    Icons.camera_alt,
                                    size: 30,
                                    color: Colors.white,
                                  ),
                                  Text(
                                    'Change Profile Photo',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      GestureDetector(
                        onTap: saveProfile,
                        child: Container(
                          width: 100,
                          height: 35,
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: kPrimaryColor,
                          ),
                          child: Center(
                            child: Text(
                              'Save',
                              style: TextStyle(
                                fontSize: 17,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  Form(
                    key: _formKey,
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(height: 30),
                          Text(
                            "User Info",
                            style: GoogleFonts.openSans(
                              textStyle: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                          inputUserName,
                          inputName,
                          inputHp,
                          inputAddress,
                          SizedBox(height: 30),
                          Text(
                            "Change Login Credentials",
                            style: GoogleFonts.openSans(
                              textStyle: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                          Text(""),
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => Reauthenticate(
                                        changes: 'email',
                                      )));
                            },
                            child: Text(
                              "Change Email",
                              style: GoogleFonts.openSans(
                                textStyle: TextStyle(
                                  decoration: TextDecoration.underline,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ),
                          ),
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => Reauthenticate(
                                        changes: 'password',
                                      )));
                            },
                            child: Text(
                              "Change Password",
                              style: GoogleFonts.openSans(
                                textStyle: TextStyle(
                                  decoration: TextDecoration.underline,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ),
                          ),
                          Text(""),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(height: 30),
                              _isLoading
                                  ? CircularProgressIndicator(
                                      valueColor:
                                          AlwaysStoppedAnimation(kPrimaryColor),
                                    )
                                  : SizedBox.shrink()
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
