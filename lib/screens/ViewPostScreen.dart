import 'package:combi_mobile/controller/FeedController.dart';
import 'package:combi_mobile/controller/UserController.dart';
import 'package:combi_mobile/models/Feed.dart';
import 'package:combi_mobile/models/Member.dart';
import 'package:combi_mobile/utilities/styling.dart';
import 'package:combi_mobile/widgets/Background.dart';
import 'package:combi_mobile/widgets/PostView.dart';
import 'package:flutter/material.dart';

class ViewPostScreen extends StatefulWidget {
  final String feedID;
  final String currentUserId;

  ViewPostScreen({this.feedID, this.currentUserId});

  @override
  _ViewPostScreenState createState() => _ViewPostScreenState();
}

class _ViewPostScreenState extends State<ViewPostScreen> {
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          'Post',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Background(
        child: FutureBuilder(
          future:
              FeedController.getFeedById(widget.currentUserId, widget.feedID),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (!snapshot.hasData) {
              return Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(kPrimaryColor),
                ),
              );
            }
            Feed feed = snapshot.data;
            return FutureBuilder(
              future: UserController.getUserWithId(feed.authorId),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (!snapshot.hasData) {
                  return SizedBox.shrink();
                }
                Member author = snapshot.data;
                return PostView(
                  currentUserId: widget.currentUserId,
                  feed: feed,
                  author: author,
                );
              },
            );
          },
        ),
      ),
    );
  }
}
