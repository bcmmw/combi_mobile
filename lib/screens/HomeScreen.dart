import 'package:combi_mobile/screens/EventScreen.dart';
import 'package:combi_mobile/screens/StartActivity.dart';
import 'package:combi_mobile/screens/screens.dart';
import 'package:combi_mobile/utilities/styling.dart';
import 'package:combi_mobile/widgets/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class HomeScreen extends StatefulWidget {
  final String currentUserId;

  const HomeScreen({Key key, this.currentUserId}) : super(key: key);
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final List<String> _titles = const [
    'ACTIVITY',
    'EVENT',
    'FEEDS',
    'TEAM',
    'ME',
  ];

  int _selectedTab = 2;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          title: Text(
            _titles[_selectedTab],
            style: kBottomNavigationBarStyle,
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.only(),
              child: IconButton(
                icon: Icon(Icons.notifications),
                onPressed: () {
                  //print('Notifications');
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => NotificationScreen(
                            currentUserId: widget.currentUserId,
                          )));
                },
              ),
            )
          ],
        ),
        body: [
          StartActivity(
            currentUserId: widget.currentUserId,
          ),
          EventScreen(
            currentUserId: widget.currentUserId,
          ),
          FeedScreen(
            currentUserId: widget.currentUserId,
          ),
          TeamScreen(
            currentUserId: widget.currentUserId,
          ),
          ProfileScreen(
            currentUserId: widget.currentUserId,
            visitedUserId: widget.currentUserId,
          ),
        ].elementAt(_selectedTab),
        bottomNavigationBar: CupertinoTabBar(
          onTap: (index) {
            setState(() {
              _selectedTab = index;
            });
          },
          activeColor: kPrimaryColor,
          currentIndex: _selectedTab,
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.directions_walk), label: _titles[0]),
            BottomNavigationBarItem(icon: Icon(Icons.event), label: _titles[1]),
            BottomNavigationBarItem(
                icon: Icon(Icons.groups), label: _titles[2]),
            BottomNavigationBarItem(
                icon: Icon(Icons.flag_sharp), label: _titles[3]),
            BottomNavigationBarItem(
                icon: Icon(Icons.person), label: _titles[4]),
          ],
        ),
      ),
    );
  }

  //Show Dialog 'Are you sure want to exit AePad?' when Back button was pressed
  Future<bool> _onBackPressed() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(
          'Are you sure want to exit COMBI?',
          style: GoogleFonts.openSans(
            textStyle: TextStyle(
              fontSize: 17,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        actions: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RoundedButton(
                btnText: 'Yes',
                width: 100,
                height: 50,
                onBtnPressed: () {
                  Navigator.pop(context, true);
                },
              ),
              SizedBox(width: 8),
              RoundedButton(
                btnText: 'No',
                width: 100,
                height: 50,
                onBtnPressed: () {
                  Navigator.pop(context, false);
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
