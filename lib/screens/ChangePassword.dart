import 'package:combi_mobile/services/auth_service.dart';
import 'package:combi_mobile/utilities/styling.dart';
import 'package:combi_mobile/widgets/widgets.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class ChangePassword extends StatefulWidget {
  final UserCredential authResult;

  const ChangePassword({Key key, this.authResult}) : super(key: key);
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  bool _obscureText = true;
  bool _obscureText1 = true;
  final _formKey = new GlobalKey<FormState>();
  dynamic _cPassword = TextEditingController();
  dynamic _cConfirmPassword = TextEditingController();

  void _toggle() {
    setState(() => _obscureText = !_obscureText);
  }

  void _toggle1() {
    setState(() => _obscureText1 = !_obscureText1);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'Change Email or Password',
          style: kBottomNavigationBarStyle,
        ),
      ),
      body: Background(
        child: Container(
          child: Column(
            children: [
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Text(
                  "Enter new email/ password",
                  style: GoogleFonts.openSans(
                    textStyle: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 15),
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        SizedBox(height: 15),
                        TextFormField(
                          controller: _cPassword,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(50),
                          ],
                          keyboardType: TextInputType.text,
                          obscureText: _obscureText,
                          decoration:
                              inputDecoration('New Password', Icons.lock,
                                  suffixIcon: InkWell(
                                    onTap: _toggle,
                                    child: Icon(
                                      _obscureText
                                          ? FontAwesomeIcons.eye
                                          : FontAwesomeIcons.eyeSlash,
                                      size: 15.0,
                                      color: Colors.black,
                                    ),
                                  )),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Required';
                            } else if (value.length < 6) {
                              return 'Password too short. Must more than 6 characters';
                            }
                            return null;
                          },
                        ),
                        TextFormField(
                          controller: _cConfirmPassword,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(50),
                          ],
                          keyboardType: TextInputType.text,
                          obscureText: _obscureText1,
                          decoration:
                              inputDecoration('Confirm Password', Icons.lock,
                                  suffixIcon: InkWell(
                                    onTap: _toggle1,
                                    child: Icon(
                                      _obscureText1
                                          ? FontAwesomeIcons.eyeSlash
                                          : FontAwesomeIcons.eye,
                                      size: 15.0,
                                      color: Colors.black,
                                    ),
                                  )),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Required';
                            } else if (value.isNotEmpty) {
                              if (value != _cPassword.text) {
                                return 'Password does not match';
                              }
                            }
                            return null;
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(height: 15),
              RoundedButton(
                btnText: 'SAVE',
                onBtnPressed: () async {
                  if (_formKey.currentState.validate()) {
                    bool isValid = await AuthService.updatePassword(
                      _cPassword.text,
                    );
                    if (isValid) {
                      alertDialog(
                        context,
                        'Changes saved successfully, please login again.',
                        () {
                          AuthService.logout();
                          Navigator.of(context)
                              .popUntil((route) => route.isFirst);
                        },
                      );
                    } else {
                      snackBar(context, AuthService.errorMsg, Colors.redAccent);
                    }
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
// class Reauthenticate extends StatelessWidget {
//   void _toggle() {
//     setState(() => _obscureText = !_obscureText);
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: TextFormField(
//         controller: _cPassword,
//         inputFormatters: [
//           LengthLimitingTextInputFormatter(50),
//         ],
//         keyboardType: TextInputType.text,
//         obscureText: _obscureText,
//         decoration: inputDecoration('New Password', Icons.lock,
//             suffixIcon: InkWell(
//               onTap: _toggle,
//               child: Icon(
//                 _obscureText ? FontAwesomeIcons.eye : FontAwesomeIcons.eyeSlash,
//                 size: 15.0,
//                 color: Colors.black,
//               ),
//             )),
//         validator: (value) {
//           if (value.isEmpty) {
//             return 'Required';
//           } else if (value.length < 6) {
//             return 'Password too short. Must more than 6 characters';
//           }
//           return null;
//         },
//       ),
//     );
//   }
// }
