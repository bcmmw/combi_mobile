import 'package:combi_mobile/screens/RegistrationForm.dart';
import 'package:combi_mobile/screens/LoginForm.dart';
import 'package:combi_mobile/utilities/styling.dart';
import 'package:combi_mobile/widgets/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen>
    with SingleTickerProviderStateMixin {
  int _selectedTab = 0;
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor:
                Colors.transparent, // Colors.white.withOpacity(0.1),
            elevation: 0,
            bottom: TabBar(
              indicatorColor: kPrimaryColor,
              indicatorPadding: const EdgeInsets.symmetric(horizontal: 20),
              labelColor: kPrimaryColor,
              unselectedLabelColor: Colors.grey,
              labelStyle: GoogleFonts.openSans(
                textStyle: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w700,
                ),
              ),
              controller: _tabController,
              tabs: [
                Tab(
                  text: "Log In",
                ),
                Tab(
                  text: "Sign Up",
                )
              ],
            ),
          ),
          body: Background(
            child: TabBarView(
              controller: _tabController,
              children: [
                LoginForm(),
                RegistrationForm(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //SHow Dialog 'Are you sure want to exit AePad?' when Back button was pressed
  Future<bool> _onBackPressed() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(
          'Are you sure want to exit COMBI?',
          style: GoogleFonts.openSans(
            textStyle: TextStyle(
              fontSize: 17,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        actions: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RoundedButton(
                btnText: 'Yes',
                width: 100,
                height: 50,
                onBtnPressed: () {
                  Navigator.pop(context, true);
                },
              ),
              SizedBox(width: 8),
              RoundedButton(
                btnText: 'No',
                width: 100,
                height: 50,
                onBtnPressed: () {
                  Navigator.pop(context, false);
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
