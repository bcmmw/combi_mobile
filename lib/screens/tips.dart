import 'package:combi_mobile/config/palette.dart';
import 'package:combi_mobile/screens/WelcomeScreen.dart';
import 'package:combi_mobile/screens/screens.dart';
import 'package:combi_mobile/widgets/widgets.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Tips extends StatefulWidget {
  Tips({Key key}) : super(key: key);

  @override
  _TipsState createState() => _TipsState();
}

class _TipsState extends State<Tips> {
  PageController _pageController = new PageController();
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: null,
        child: Scaffold(
          backgroundColor: const Color(0xffffffff),
          body: Stack(
            children: [
              Background(),
              PageView(
                controller: _pageController,
                children: [
                  _tips(
                      'DO YOU KNOW?',
                      'Dengue fever is caused by dengue virus. Every year about 100 million cases of dengue occur in worldwide.',
                      'NEXT'),
                  _tips(
                      'DENGUE PREVENTION',
                      'We can prevent dengue by spending at least 10 minutes over the weekend looking for and destroying Aedes breeding grounds',
                      'LET\'S GO'),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.all(70),
                    child: Center(
                      child: SmoothPageIndicator(
                        controller: _pageController,
                        count: 2,
                        effect: SlideEffect(
                            spacing: 10.0,
                            radius: 100,
                            dotWidth: 15.0,
                            dotHeight: 15.0,
                            paintStyle: PaintingStyle.fill,
                            strokeWidth: 1.5,
                            dotColor: Colors.black45,
                            activeDotColor: Palette.primaryColor),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ));
  }

  Stack _tips(String title, String content, String button) {
    return Stack(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Transform.translate(
              offset: Offset(0, 185.0),
              child: Container(
                width: 214.0,
                height: 271.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(28.0),
                  color: Palette.primaryColor,
                  boxShadow: [
                    BoxShadow(
                      color: const Color(0x29000000),
                      offset: Offset(5, 9),
                      blurRadius: 5,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Transform.translate(
              offset: Offset(0, 247.0),
              child: SizedBox(
                  width: 191.0,
                  height: 147.0,
                  child: _buildText(
                      content, 18, Color(0xb2000000), 1.1111111111111112)),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Transform.translate(
              offset: Offset(0, 93.0),
              child: SizedBox(
                width: 350.0,
                child: _buildText(
                    title, 28, Color(0xff000000), 0.7142857142857143),
              ),
            ),
          ],
        ),
        Align(
          alignment: Alignment.center,
          child: Transform.translate(
            offset: Offset(0, 180.0),
            child: InkWell(
              onTap: () {
                print('clicked');
                _pageController.page == 0
                    ? _pageController.animateToPage(1,
                        curve: Curves.ease, duration: Duration(seconds: 1))
                    : Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => WelcomeScreen()));
              },
              child: Container(
                width: 95.0,
                height: 30.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12.0),
                  color: Palette.primaryColor,
                  boxShadow: [
                    BoxShadow(
                      color: const Color(0x29000000),
                      offset: Offset(0, 0),
                      blurRadius: 99,
                    ),
                  ],
                ),
                child: SizedBox(
                  width: 82.0,
                  height: 17.0,
                  child: _buildText(button, 15, Color(0xff000000), null),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Text _buildText(String text, double size, Color color, double height) {
    return Text(
      text,
      style: GoogleFonts.robotoSlab(
        textStyle: TextStyle(
          fontSize: size,
          color: const Color(0xff000000),
          fontWeight: FontWeight.w700,
          height: height,
        ),
      ),
      textHeightBehavior: TextHeightBehavior(applyHeightToFirstAscent: true),
      textAlign: TextAlign.center,
    );
  }
}
