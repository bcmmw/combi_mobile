import 'package:combi_mobile/controller/EventController.dart';
import 'package:combi_mobile/controller/UserController.dart';
import 'package:combi_mobile/models/Event.dart';
import 'package:combi_mobile/models/Member.dart';
import 'package:combi_mobile/utilities/styling.dart';
import 'package:combi_mobile/widgets/Background.dart';
import 'package:combi_mobile/widgets/EventContainer.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class EventScreen extends StatefulWidget {
  final String currentUserId;

  const EventScreen({Key key, this.currentUserId}) : super(key: key);
  @override
  _EventScreenState createState() => _EventScreenState();
}

class _EventScreenState extends State<EventScreen>
    with SingleTickerProviderStateMixin {
  Member _currentUser;
  List<Event> _teamEvents = [];
  int _selectedIndex = 0;
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);

    _tabController.addListener(() {
      setState(() {
        _selectedIndex = _tabController.index;
      });
      print("Selected Index: " + _tabController.index.toString());
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent, // Colors.white.withOpacity(0.1),
          elevation: 0,
          bottom: TabBar(
            indicatorColor: kPrimaryColor,
            indicatorPadding: const EdgeInsets.symmetric(horizontal: 10),
            labelColor: kPrimaryColor,
            unselectedLabelColor: Colors.grey,
            labelStyle: GoogleFonts.openSans(
              textStyle: TextStyle(
                fontSize: 17,
                fontWeight: FontWeight.w700,
              ),
            ),
            controller: _tabController,
            tabs: [
              Tab(
                text: "Latest Events",
              ),
              Tab(
                text: "Ongoing Events",
              ),
              Tab(
                text: "Past Events",
              )
            ],
          ),
        ),
        body: Background(
          child: StreamBuilder(
            stream: UserController.getUserStreamWithId(widget.currentUserId),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (!snapshot.hasData) {
                return SizedBox.shrink();
              }
              _currentUser = snapshot.data;
              print(_tabController.index);
              return ListView(
                physics: const BouncingScrollPhysics(
                    parent: AlwaysScrollableScrollPhysics()),
                children: [
                  _currentUser.teamID.isEmpty
                      ? Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(height: 30),
                            Text(
                              'Please join a COMBI team first... ',
                              style: GoogleFonts.robotoSlab(
                                textStyle: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                            SizedBox(height: 20),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 25),
                              child: Text(
                                'You can view and participate in the event organized by your team to earn more point and get more reward!',
                                style: GoogleFonts.robotoSlab(
                                  textStyle: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        )
                      : StreamBuilder(
                          stream: EventController.getTeamEvents(
                              _currentUser.teamID, _tabController.index),
                          builder:
                              (BuildContext context, AsyncSnapshot snapshot) {
                            if (!snapshot.hasData) {
                              return SizedBox.shrink();
                            }
                            _teamEvents = snapshot.data;
                            if (_teamEvents.length > 0) {
                              return SizedBox(
                                height: MediaQuery.of(context).size.height,
                                child: ListView.builder(
                                  itemCount: _teamEvents.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    Event event = _teamEvents[index];

                                    return EventContainer(
                                        event: event,
                                        currentUserId: widget.currentUserId,
                                        teamId: _currentUser.teamID);
                                  },
                                ),
                              );
                            } else {
                              return Column(
                                children: [
                                  SizedBox(height: 30),
                                  Text(
                                    'No recent events organized by your team... ',
                                    style: GoogleFonts.robotoSlab(
                                      textStyle: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                  ),
                                ],
                              );
                            }
                          },
                        )
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
