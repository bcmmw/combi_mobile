import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:combi_mobile/controller/ActivityController.dart';
import 'package:combi_mobile/controller/UserController.dart';
import 'package:combi_mobile/models/Activity.dart';
import 'package:combi_mobile/services/StorageService.dart';
import 'package:combi_mobile/utilities/styling.dart';
import 'package:combi_mobile/widgets/Background.dart';
import 'package:combi_mobile/widgets/RoundedButton.dart';
import 'package:combi_mobile/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'dart:math' as math;
import 'dart:io';

import 'package:image_picker/image_picker.dart';

class ActivityPage extends StatefulWidget {
  final String currentUserId;

  const ActivityPage({Key key, this.currentUserId}) : super(key: key);

  @override
  _ActivityPageState createState() => _ActivityPageState();
}

class _ActivityPageState extends State<ActivityPage>
    with TickerProviderStateMixin {
  AnimationController controller;
  Duration _duration = Duration(seconds: 10);
  int _pointGained = 50;
  File _imageBefore;
  File _imageAfter;
  bool _isPlaying = false;
  bool _visible = true;
  bool _isLoading = false;

  String get timerString {
    Duration duration = controller.duration * controller.value;
    return '${duration.inMinutes.toString().padLeft(2, '0')}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
  }

  _handleImage(bool _before) async {
    //Navigator.pop(context);
    final picker = ImagePicker();
    File imageFile;
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    if (pickedFile != null) {
      imageFile = File(pickedFile.path);
      //imageFile = await _cropImage(imageFile);

      // void setState(fn) {
      //   if (mounted) {
      //     super.setState(fn);
      //   }
      // }

      if (mounted) {
        setState(() {
          _before ? _imageBefore = imageFile : _imageAfter = imageFile;
        });
      }

      //   setState(() {
      //     _image = imageFile;
      //   });
    }
  }

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: _duration,
    );

    @override
    void dispose() {
      controller.dispose();
      super.dispose();
    }
    // ..addStatusListener((status) {
    //     if (controller.status == AnimationStatus.dismissed) {
    //       setState(() => isPlaying = false);
    //     }

    //     print(status);
    //   })
  }

  _submit() async {
    int count = 0;
    if (!_isLoading && _imageBefore != null && _imageAfter != null) {
      setState(() {
        _isLoading = true;
      });

      // Create post
      String _beforeImageUrl =
          await StorageService.uploadActivityPicture(_imageBefore);
      String _afterImageUrl =
          await StorageService.uploadActivityPicture(_imageAfter);

      Activity activity = Activity(
        beforeImageUrl: _beforeImageUrl,
        afterImageUrl: _afterImageUrl,
        memberId: widget.currentUserId,
        duration: _duration.inSeconds,
        timestamp: Timestamp.fromDate(DateTime.now()),
      );
      ActivityController.createActivity(activity);

      setState(() {
        _isLoading = false;
      });

      // Update xp point and level
      UserController.levelUp(_pointGained, widget.currentUserId);

      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Congratulations! You have gained $_pointGained points.',
                style: GoogleFonts.openSans(
                  textStyle: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
            ],
          ),
          actions: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                RoundedButton(
                    btnText: 'OK',
                    width: 100,
                    height: 50,
                    onBtnPressed: () =>
                        Navigator.of(context).popUntil((_) => count++ >= 2)),
              ],
            ),
          ],
        ),
      );
    } else {
      snackBar(context, 'Error: Please check again.', Colors.redAccent);
    }
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    ThemeData themeData = Theme.of(context);
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            'Activity',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ),
        body: Background(
          child: Padding(
            padding: EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                _isLoading
                    ? Padding(
                        padding: EdgeInsets.only(bottom: 10.0),
                        child: LinearProgressIndicator(
                          backgroundColor: Colors.amber[100],
                          valueColor: AlwaysStoppedAnimation(kPrimaryColor),
                        ),
                      )
                    : SizedBox.shrink(),
                Expanded(
                  child: Align(
                    alignment: FractionalOffset.center,
                    child: AspectRatio(
                      aspectRatio: 1.0,
                      child: Stack(
                        children: <Widget>[
                          Positioned.fill(
                            child: AnimatedBuilder(
                              animation: controller,
                              builder: (BuildContext context, Widget child) {
                                return CustomPaint(
                                    painter: TimerPainter(
                                  animation: controller,
                                  backgroundColor: Colors.grey[300],
                                  color: kPrimaryColor,
                                ));
                              },
                            ),
                          ),
                          Align(
                            alignment: FractionalOffset.center,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  "Count Down",
                                  style: themeData.textTheme.headline5,
                                ),
                                AnimatedBuilder(
                                    animation: controller,
                                    builder:
                                        (BuildContext context, Widget child) {
                                      if (controller.isAnimating ||
                                          _isPlaying) {
                                        return Text(
                                          timerString,
                                          style: themeData.textTheme.headline3,
                                        );
                                      } else {
                                        return Text(
                                          '${controller.duration.inMinutes.toString().padLeft(2, '0')}:${(controller.duration.inSeconds % 60).toString().padLeft(2, '0')}',
                                          style: themeData.textTheme.headline3,
                                        );
                                      }
                                      // return controller.isAnimating
                                      //     ? Text(
                                      //         timerString,
                                      //         style:
                                      //             themeData.textTheme.headline3,
                                      //       )
                                      //     : Text(
                                      //         '${controller.duration.inMinutes}:${(controller.duration.inSeconds % 60).toString().padLeft(2, '0')}',
                                      //         style:
                                      //             themeData.textTheme.headline3,
                                      //       );
                                    }),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Visibility(
                        visible: _visible,
                        child: FloatingActionButton(
                          child: AnimatedBuilder(
                            animation: controller,
                            builder: (BuildContext context, Widget child) {
                              return Icon(controller.isAnimating
                                  ? Icons.pause
                                  : Icons.play_arrow);

                              // Icon(isPlaying
                              // ? Icons.pause
                              // : Icons.play_arrow);
                            },
                          ),
                          onPressed: () {
                            // setState(() => isPlaying = !isPlaying);

                            if (controller.isAnimating) {
                              controller.stop(canceled: true);
                            } else {
                              controller.reverse(
                                  from: controller.value == 0.0
                                      ? 1.0
                                      : controller.value);
                            }
                            _isPlaying = true;
                            setState(() => _visible = false);
                          },
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Text(
                        'Before',
                        style: themeData.textTheme.headline5,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Text(
                        'After',
                        style: themeData.textTheme.headline5,
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: GestureDetector(
                        onTap: () => _handleImage(true),
                        child: Container(
                          height: width / 2.2,
                          width: width / 2.2,
                          decoration: BoxDecoration(
                            color: Colors.grey[300],
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: _imageBefore == null
                              ? Icon(
                                  Icons.add_a_photo,
                                  color: Colors.white70,
                                  size: 150.0,
                                )
                              :
                              // DecorationImage(
                              //       fit: BoxFit.cover,
                              //       image: FileImage(_image),
                              //     ),
                              Image(
                                  image: FileImage(_imageBefore),
                                  fit: BoxFit.cover,
                                ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: GestureDetector(
                        onTap: () => _handleImage(false),
                        child: Container(
                          height: width / 2.2,
                          width: width / 2.2,
                          decoration: BoxDecoration(
                            color: Colors.grey[300],
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: _imageAfter == null
                              ? Icon(
                                  Icons.add_a_photo,
                                  color: Colors.white70,
                                  size: 150.0,
                                )
                              :
                              // DecorationImage(
                              //       fit: BoxFit.cover,
                              //       image: FileImage(_image),
                              //     ),
                              Image(
                                  image: FileImage(_imageAfter),
                                  fit: BoxFit.cover,
                                ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                RoundedButton(
                    btnText: 'Finish',
                    onBtnPressed: () => {
                          if (!_isPlaying)
                            {
                              alertDialog(context, 'Start the timer now.',
                                  () => Navigator.pop(context))
                            }
                          else if (_isPlaying && timerString != '00:00')
                            {
                              alertDialog(
                                  context,
                                  'It\'s not 10 minutes yet. \n\nComplete 10 minutes to level up and get more rewards!',
                                  () => Navigator.pop(context))
                            }
                          else
                            {
                              if (_imageBefore == null || _imageAfter == null)
                                {
                                  alertDialog(
                                      context,
                                      'BEFORE & AFTER pic should not be empty.',
                                      () => Navigator.pop(context))
                                }
                              else
                                {
                                  _submit(),
                                  // print(
                                  // Timestamp.fromDate(DateTime.parse(
                                  //     'T00:${controller.duration.inMinutes.toString().padLeft(2, '0')}:${(controller.duration.inSeconds % 60).toString().padLeft(2, '0')}')),

                                  print('Duration: ' +
                                      (10 -
                                              controller.duration.inMinutes *
                                                  controller.value)
                                          .toString()),
                                }
                            }
                        })
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(
          'Are you sure want to exit? \n\nThis activity will not be saved.',
          style: GoogleFonts.openSans(
            textStyle: TextStyle(
              fontSize: 17,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        actions: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RoundedButton(
                btnText: 'Yes',
                width: 100,
                height: 50,
                onBtnPressed: () {
                  Navigator.pop(context, true);
                },
              ),
              SizedBox(width: 8),
              RoundedButton(
                btnText: 'No',
                width: 100,
                height: 50,
                onBtnPressed: () {
                  Navigator.pop(context, false);
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class TimerPainter extends CustomPainter {
  TimerPainter({
    this.animation,
    this.backgroundColor,
    this.color,
  }) : super(repaint: animation);

  final Animation<double> animation;
  final Color backgroundColor, color;

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = backgroundColor
      ..strokeWidth = 5.0
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke;

    canvas.drawCircle(size.center(Offset.zero), size.width / 2.0, paint);
    paint.color = color;
    double progress = (1.0 - animation.value) * 2 * math.pi;
    canvas.drawArc(Offset.zero & size, math.pi * 1.5, -progress, false, paint);
  }

  @override
  bool shouldRepaint(TimerPainter old) {
    return animation.value != old.animation.value ||
        color != old.color ||
        backgroundColor != old.backgroundColor;
  }
}
