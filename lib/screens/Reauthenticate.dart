import 'package:combi_mobile/screens/ChangeEmail.dart';
import 'package:combi_mobile/screens/ChangePassword.dart';
import 'package:combi_mobile/services/auth_service.dart';
import 'package:combi_mobile/utilities/styling.dart';
import 'package:combi_mobile/widgets/widgets.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class Reauthenticate extends StatefulWidget {
  final String changes;

  const Reauthenticate({Key key, this.changes}) : super(key: key);
  @override
  _ReauthenticateState createState() => _ReauthenticateState();
}

class _ReauthenticateState extends State<Reauthenticate> {
  bool _obscureText = true;
  final _formKey = new GlobalKey<FormState>();
  dynamic _cPassword = TextEditingController();
  void _toggle() {
    setState(() => _obscureText = !_obscureText);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'Verify Password',
          style: kBottomNavigationBarStyle,
        ),
      ),
      body: Background(
        child: Container(
          child: Column(
            children: [
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Text(
                  "Keep your account safe, please verify your identity by entering your password.",
                  style: GoogleFonts.openSans(
                    textStyle: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 15),
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Form(
                    key: _formKey,
                    child: TextFormField(
                      controller: _cPassword,
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(50),
                      ],
                      keyboardType: TextInputType.text,
                      obscureText: _obscureText,
                      decoration:
                          inputDecoration('Current Password', Icons.lock,
                              suffixIcon: InkWell(
                                onTap: _toggle,
                                child: Icon(
                                  _obscureText
                                      ? FontAwesomeIcons.eye
                                      : FontAwesomeIcons.eyeSlash,
                                  size: 15.0,
                                  color: Colors.black,
                                ),
                              )),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Required';
                        } else if (value.length < 6) {
                          return 'Password too short. Must more than 6 characters';
                        }
                        return null;
                      },
                    ),
                  ),
                ),
              ),
              SizedBox(height: 15),
              RoundedButton(
                btnText: 'CONTINUE',
                onBtnPressed: () async {
                  if (_formKey.currentState.validate()) {
                    UserCredential authResult =
                        await AuthService.reauthenticate(
                      _cPassword.text,
                    );
                    if (authResult != null) {
                      if (widget.changes == 'email') {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) =>
                                ChangeEmail(authResult: authResult)));
                      } else if (widget.changes == 'password') {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) =>
                                ChangePassword(authResult: authResult)));
                      }
                    } else {
                      snackBar(context, AuthService.errorMsg, Colors.redAccent);
                    }
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
// class Reauthenticate extends StatelessWidget {
//   void _toggle() {
//     setState(() => _obscureText = !_obscureText);
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: TextFormField(
//         controller: _cPassword,
//         inputFormatters: [
//           LengthLimitingTextInputFormatter(50),
//         ],
//         keyboardType: TextInputType.text,
//         obscureText: _obscureText,
//         decoration: inputDecoration('New Password', Icons.lock,
//             suffixIcon: InkWell(
//               onTap: _toggle,
//               child: Icon(
//                 _obscureText ? FontAwesomeIcons.eye : FontAwesomeIcons.eyeSlash,
//                 size: 15.0,
//                 color: Colors.black,
//               ),
//             )),
//         validator: (value) {
//           if (value.isEmpty) {
//             return 'Required';
//           } else if (value.length < 6) {
//             return 'Password too short. Must more than 6 characters';
//           }
//           return null;
//         },
//       ),
//     );
//   }
// }
