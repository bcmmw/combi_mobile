import 'package:combi_mobile/controller/TeamController.dart';
import 'package:combi_mobile/controller/UserController.dart';
import 'package:combi_mobile/models/CombiTeam.dart';
import 'package:combi_mobile/models/Member.dart';
import 'package:combi_mobile/services/levelExp_service.dart';
import 'package:combi_mobile/utilities/styling.dart';
import 'package:combi_mobile/widgets/Background.dart';
import 'package:combi_mobile/widgets/RoundedButton.dart';
import 'package:combi_mobile/widgets/TeamInfoContainer.dart';
import 'package:combi_mobile/widgets/TeamRankingContainer.dart';
import 'package:flutter/material.dart';
import 'dart:ui' as ui;

class TeamScreen extends StatefulWidget {
  final String currentUserId;

  const TeamScreen({Key key, this.currentUserId}) : super(key: key);

  @override
  _TeamScreenState createState() => _TeamScreenState();
}

class _TeamScreenState extends State<TeamScreen> {
  double _currentSliderValue = 0;
  Member _currentUser;
  List<CombiTeam> _teams = [];
  List<CombiTeam> _teamRanking = [];
  CombiTeam _userTeam = new CombiTeam();

  void getTeamRanking() async {
    List<CombiTeam> teamRanking = await TeamController.getRanking();
    if (mounted) {
      setState(() {
        _teamRanking = teamRanking;
      });
    }
  }

  void initState() {
    super.initState();
    getTeamRanking();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Background(
        child: StreamBuilder(
          stream: UserController.getUserStreamWithId(widget.currentUserId),
          //  _currentUser.teamID.isEmpty
          //     ? TeamController.getAllTeams()
          //     : TeamController.getTeamWithId(_currentUser.teamID),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            //     FutureBuilder(
            // future: _currentUser.teamID.isEmpty
            //     ? TeamController.getAllTeams()
            //     : TeamController.getTeamWithId(_currentUser.teamID),
            // builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (!snapshot.hasData) {
              return SizedBox.shrink();
            }
            _currentUser = snapshot.data;
            // _currentUser.teamID.isEmpty
            //     ? getAllTeams()
            //     : getTeamWithId(_currentUser.teamID);

            return ListView(
              physics: const BouncingScrollPhysics(
                  parent: AlwaysScrollableScrollPhysics()),
              children: [
                Center(
                  child: RoundedButton(
                    btnText: _currentUser.teamID.isEmpty
                        ? 'Teams Nearby'
                        : 'My Team',
                    width: 150,
                    height: 50,
                    color: Colors.black,
                  ),
                ),
                SizedBox(height: 20),
                ClipRect(
                  child: BackdropFilter(
                    filter: ui.ImageFilter.blur(sigmaX: 30.0, sigmaY: 30.0),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius: BorderRadius.circular(20),
                        //color: Colors.white,
                        border: Border.all(color: Colors.black12),
                      ),
                      child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: _currentUser.teamID.isEmpty
                              ? FutureBuilder(
                                  future: TeamController.getAllTeams1(),
                                  builder: (BuildContext context,
                                      AsyncSnapshot snapshot) {
                                    if (!snapshot.hasData) {
                                      return SizedBox.shrink();
                                    }
                                    _teams = snapshot.data;
                                    return SizedBox(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              2,
                                      child: ListView.builder(
                                        itemCount: _teams.length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          CombiTeam team = _teams[index];

                                          return TeamContainer(
                                            team: team,
                                            currentUserId: widget.currentUserId,
                                          );
                                        },
                                      ),
                                    );
                                  },
                                )

                              // Expanded(
                              //                 child: SizedBox(
                              //                   height: 225,
                              //                   child: ListView.builder(
                              //                     itemCount: _teams.length,
                              //                     itemBuilder:
                              //                         (BuildContext context, int index) {
                              //                       CombiTeam team = _teams[index];

                              //                       return TeamContainer(
                              //                         team: team,
                              //                         currentUserId: widget.currentUserId,
                              //                       );
                              //                     },
                              //                   ),
                              //                 ),
                              //               )
                              : FutureBuilder(
                                  future: TeamController.getTeamWithId1(
                                      _currentUser.teamID),
                                  builder: (BuildContext context,
                                      AsyncSnapshot snapshot) {
                                    if (!snapshot.hasData) {
                                      return SizedBox.shrink();
                                    }
                                    _userTeam = snapshot.data;
                                    return Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: [
                                            CircleAvatar(
                                              radius: 45,
                                              backgroundImage: _userTeam
                                                      .image.isEmpty
                                                  ? AssetImage(
                                                      'assets/images/placeholder.png')
                                                  : NetworkImage(
                                                      _userTeam.image),
                                            ),
                                            SizedBox(width: 15.0),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                SizedBox(height: 10),
                                                Text(
                                                  _userTeam.name,
                                                  style: TextStyle(
                                                    fontSize: 20,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                                FutureBuilder(
                                                  future: TeamController
                                                      .getTeamMembers(
                                                          _userTeam.id),
                                                  builder: (BuildContext
                                                          context,
                                                      AsyncSnapshot snapshot) {
                                                    if (!snapshot.hasData) {
                                                      return SizedBox.shrink();
                                                    }
                                                    int totalMembers =
                                                        snapshot.data.length;
                                                    return Text(
                                                      totalMembers.toString() +
                                                          ' member',
                                                      style: TextStyle(
                                                        fontSize: 15,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                      ),
                                                    );
                                                  },
                                                ),
                                                SizedBox(height: 5),
                                              ],
                                            ),
                                          ],
                                        ),

                                        SizedBox(height: 15),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              ' Level ' +
                                                  _userTeam.level.toString(),
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500,
                                                letterSpacing: 2,
                                              ),
                                            ),
                                            Text(
                                              _userTeam.point.toString() +
                                                  '/' +
                                                  LevelExpService
                                                          .calculateLevelExp(
                                                              _userTeam.level)
                                                      .toString(),
                                              style: TextStyle(
                                                fontSize: 13,
                                              ),
                                            ),
                                          ],
                                        ),
                                        Slider(
                                          value: _currentSliderValue,
                                          min: 0,
                                          max:
                                              LevelExpService.calculateLevelExp(
                                                      _userTeam.level)
                                                  .toDouble(),
                                          label: _currentSliderValue.toString(),
                                          activeColor: kPrimaryColor,
                                          inactiveColor: Colors.amber[100],
                                          onChanged: (double value) {
                                            setState(() {
                                              _currentSliderValue = value;
                                            });
                                          },
                                        ),
                                        // Container(
                                        //   width: MediaQuery.of(context).size.width,
                                        //   child: CupertinoSlidingSegmentedControl(
                                        //     groupValue: _profileSegmentedValue,
                                        //     thumbColor: kPrimaryColor,
                                        //     backgroundColor: Colors.blueGrey,
                                        //     children: _profileTabs,
                                        //     onValueChanged: (i) {
                                        //       setState(() {
                                        //         _profileSegmentedValue = i;
                                        //       });
                                        //     },
                                        //   ),
                                        // )
                                      ],
                                    );
                                  },
                                )),
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Center(
                  child: RoundedButton(
                    btnText: 'Team Ranking',
                    width: 150,
                    height: 50,
                    color: Colors.black,
                  ),
                ),
                SizedBox(height: 10),
                Expanded(
                  child: SizedBox(
                    height: MediaQuery.of(context).size.height / 2,
                    child: ListView.builder(
                      itemCount: _teamRanking.length,
                      itemBuilder: (BuildContext context, int index) {
                        CombiTeam team = _teamRanking[index];

                        return TeamRankingContainer(
                          team: team,
                          rank: index + 1,
                        );
                      },
                    ),
                  ),
                ),
                // StreamBuilder(
                //   stream: TeamController.getRanking(),
                //   builder: (BuildContext context, AsyncSnapshot snapshot) {
                //     if (!snapshot.hasData) {
                //       return SizedBox.shrink();
                //     }
                //     final List<CombiTeam> teamRanking = snapshot.data;
                //     return ListView.builder(
                //       physics: const BouncingScrollPhysics(
                //           parent: AlwaysScrollableScrollPhysics()),
                //       itemCount: teamRanking.length,
                //       itemBuilder: (BuildContext context, int index) {
                //         CombiTeam team = teamRanking[index];
                //         return FutureBuilder(
                //           future: UserController.getUserWithId(
                //               widget.currentUserId),
                //           builder:
                //               (BuildContext context, AsyncSnapshot snapshot) {
                //             if (!snapshot.hasData) {
                //               return SizedBox.shrink();
                //             }
                //             return TeamRankingContainer(
                //               currentTeamId: team.id,
                //               team: team,
                //               rank: index + 1,
                //             );
                //           },
                //         );
                //       },
                //     );
                //   },
                // ),
              ],
            );
          },
        ),
      ),
    );
  }
}
