import 'dart:async';
import 'package:combi_mobile/screens/screens.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  final String currentUserId;
  SplashScreen({Key key, this.currentUserId}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  void initState() {
    super.initState();

    Timer(
        Duration(seconds: 2),
        () => Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => Tips())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/background.png'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(0, 111.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 324.0,
                  height: 62.0,
                  child: Text(
                    'DENGUE PREVENTION START WITH ME',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: 28,
                      color: const Color(0xff000000),
                      fontWeight: FontWeight.w700,
                      height: 0.8928571428571429,
                    ),
                    textHeightBehavior:
                        TextHeightBehavior(applyHeightToFirstAscent: false),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
          Transform.translate(
            offset: Offset(0, 210.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 282.0,
                  child: Text(
                    'LET\'S GET STARTED',
                    style: TextStyle(
                      fontFamily: 'Comic Sans MS',
                      fontSize: 28,
                      color: const Color(0xff000000),
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
