import 'package:cached_network_image/cached_network_image.dart';
import 'package:combi_mobile/controller/FeedController.dart';
import 'package:combi_mobile/controller/UserController.dart';
import 'package:combi_mobile/models/Comment.dart';
import 'package:combi_mobile/models/Feed.dart';
import 'package:combi_mobile/models/Member.dart';
import 'package:combi_mobile/screens/ProfileScreen.dart';
import 'package:combi_mobile/utilities/constants.dart';
import 'package:combi_mobile/utilities/styling.dart';
import 'package:combi_mobile/widgets/ConfirmDeleteDialog.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class CommentsScreen extends StatefulWidget {
  final Feed feed;
  final int likeCount;
  final int commentCount;
  final String currentUserId;

  CommentsScreen(
      {this.feed, this.likeCount, this.commentCount, this.currentUserId});

  @override
  _CommentsScreenState createState() => _CommentsScreenState();
}

class _CommentsScreenState extends State<CommentsScreen> {
  final TextEditingController _commentController = TextEditingController();
  bool _isCommenting = false;
  int _commentCount = 0;

  void initState() {
    super.initState();
    _commentCount = widget.commentCount;
  }

  _buildComment(Comment comment) {
    return FutureBuilder(
      future: UserController.getUserWithId(comment.authorId),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) {
          return SizedBox.shrink();
        }
        Member author = snapshot.data;
        return ListTile(
          leading: GestureDetector(
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => ProfileScreen(
                  currentUserId: widget.currentUserId,
                  visitedUserId: comment.authorId,
                  fromOtherPage: true,
                ),
              ),
            ),
            child: CircleAvatar(
              radius: 25.0,
              backgroundColor: Colors.grey,
              backgroundImage: author.image.isEmpty
                  ? AssetImage('assets/images/placeholder.png')
                  : CachedNetworkImageProvider(author.image),
            ),
          ),
          title: GestureDetector(
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => ProfileScreen(
                  currentUserId: widget.currentUserId,
                  visitedUserId: comment.authorId,
                  fromOtherPage: true,
                ),
              ),
            ),
            child: Text(
              author.name,
              style: GoogleFonts.robotoSlab(
                textStyle: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                    color: Colors.black87),
              ),
            ),
          ),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 6.0),
              Text(
                comment.content,
                style: GoogleFonts.openSans(
                  textStyle: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w500,
                      color: Colors.black87),
                ),
              ),
              SizedBox(height: 6.0),
              Text(
                DateFormat.yMd().add_jm().format(comment.timestamp.toDate()),
              ),
              Divider()
            ],
          ),
          trailing: widget.currentUserId == comment.authorId
              ? PopupMenuButton(
                  icon: Icon(
                    Icons.more_vert,
                    color: Colors.black45,
                    size: 30,
                  ),
                  itemBuilder: (_) {
                    return <PopupMenuItem<String>>[
                      new PopupMenuItem(
                        child: Text('Delete comment'),
                        value: 'delete',
                      )
                    ];
                  },
                  onSelected: (selectedItem) {
                    if (selectedItem == 'delete') {
                      confirmDelete(context, 'Delete comment?', () {
                        FeedController.deleteComment(
                            feed: widget.feed, comment: comment);

                        Navigator.pop(context, true);

                        --_commentCount;
                      });
                    }
                  },
                )
              : SizedBox(),
        );
      },
    );
  }

  _buildCommentTF() {
    return IconTheme(
      data: IconThemeData(
        color: _isCommenting
            ? Theme.of(context).accentColor
            : Theme.of(context).disabledColor,
      ),
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(width: 10.0),
            Expanded(
              child: TextField(
                controller: _commentController,
                textCapitalization: TextCapitalization.sentences,
                onChanged: (comment) {
                  setState(() {
                    _isCommenting = comment.length > 0;
                  });
                },
                decoration:
                    InputDecoration.collapsed(hintText: 'Write a comment...'),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 4.0),
              child: IconButton(
                icon: Icon(Icons.send),
                onPressed: () {
                  if (_isCommenting) {
                    FeedController.commentOnPost(
                      currentUserId: widget.currentUserId,
                      feed: widget.feed,
                      comment: _commentController.text,
                    );
                    _commentController.clear();
                    ++_commentCount;
                    setState(() {
                      _isCommenting = false;
                    });
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            'Comments',
            style: TextStyle(color: Colors.black),
          ),
        ),
        body: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(12.0),
              child: Text(
                '${widget.likeCount} likes',
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            StreamBuilder(
              stream: commentsRef
                  .doc(widget.feed.id)
                  .collection('feedComments')
                  .orderBy('timestamp', descending: true)
                  .snapshots(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (!snapshot.hasData) {
                  return Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation(kPrimaryColor),
                    ),
                  );
                }
                if (snapshot.data.docs.length != 0) {
                  return Expanded(
                    child: ListView.builder(
                      itemCount: snapshot.data.docs.length,
                      itemBuilder: (BuildContext context, int index) {
                        Comment comment =
                            Comment.fromDoc(snapshot.data.docs[index]);
                        return _buildComment(comment);
                      },
                    ),
                  );
                } else {
                  return Expanded(
                    child: Text(
                      'Be the first to leave comments...',
                      style: GoogleFonts.openSans(
                        textStyle: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                  );
                }
              },
            ),
            Divider(height: 1.0),
            _buildCommentTF(),
          ],
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() {
    print('on back pressed, commentCOunt = ' + _commentCount.toString());
    Navigator.pop(context, _commentCount);
  }
}
