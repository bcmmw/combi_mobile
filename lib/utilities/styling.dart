import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

//style
final Color kPrimaryColor = Color(0xffffbd33);

final TextStyle kBottomNavigationBarStyle = GoogleFonts.robotoSlab(
  textStyle: TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w700,
  ),
);

final kHintTextStyle = TextStyle(
  color: Colors.black54,
  fontFamily: 'OpenSans',
);

final kLabelStyle = TextStyle(
  color: Colors.black,
  fontWeight: FontWeight.bold,
  fontFamily: 'OpenSans',
);

final kBoxDecorationStyle = BoxDecoration(
  color: Colors.black,
  borderRadius: BorderRadius.circular(10.0),
  boxShadow: [
    BoxShadow(
      color: Colors.black12,
      blurRadius: 6.0,
      offset: Offset(0, 2),
    ),
  ],
);
