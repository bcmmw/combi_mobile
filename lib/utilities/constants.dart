import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';

final _fireStore = FirebaseFirestore.instance;

final usersRef = _fireStore.collection('users');
final feedsRef = _fireStore.collection('feeds');
final likesRef = _fireStore.collection('likes');
final commentsRef = _fireStore.collection('comments');
final activitiesRef = _fireStore.collection('activities');
final notificationsRef = _fireStore.collection('notifications');
final teamsRef = _fireStore.collection('teams');
final eventsRef = _fireStore.collection('events');

final storageRef = FirebaseStorage.instance.ref();

// const kGoogleApiKey = 'AIzaSyDM8oa9woZzAKN0wkIk7xVx0v53QLc6xL8';
