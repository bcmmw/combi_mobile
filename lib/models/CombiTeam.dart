import 'package:cloud_firestore/cloud_firestore.dart';

class CombiTeam {
  String id;
  String name;
  String address;
  String image;
  int level;
  int point;
  String leaderID;
  String officerID;

  CombiTeam(
      {this.id,
      this.name,
      this.address,
      this.image,
      this.level,
      this.point,
      this.leaderID,
      this.officerID});

  factory CombiTeam.fromDoc(DocumentSnapshot doc) {
    return CombiTeam(
      id: doc.id,
      name: doc['name'],
      address: doc['address'],
      image: doc['image'],
      level: doc['level'],
      point: doc['point'],
      leaderID: doc['leaderID'],
      officerID: doc['officerID'],
    );
  }
}
