import 'package:cloud_firestore/cloud_firestore.dart';

class Feed {
  final String id;
  final String imageUrl;
  final String caption;
  final int likeCount;
  final int commentCount;
  final String authorId;
  final Timestamp timestamp;

  Feed({
    this.id,
    this.imageUrl,
    this.caption,
    this.likeCount,
    this.commentCount,
    this.authorId,
    this.timestamp,
  });

  factory Feed.fromDoc(DocumentSnapshot doc) {
    return Feed(
      id: doc.id,
      imageUrl: doc['imageUrl'],
      caption: doc['caption'],
      likeCount: doc['likeCount'],
      commentCount: doc['commentCount'],
      authorId: doc['authorId'],
      timestamp: doc['timestamp'],
    );
  }
}
