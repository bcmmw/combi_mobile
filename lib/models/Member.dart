import 'package:cloud_firestore/cloud_firestore.dart';

class Member {
  String id;
  String username;
  String password;
  String email;
  String name;
  String hpNum;
  String address;
  String image;
  int level;
  int point;
  String teamID;

  Member({
    this.id,
    this.username,
    this.password,
    this.email,
    this.name,
    this.hpNum,
    this.address,
    this.image,
    this.level,
    this.point,
    this.teamID,
  });

  factory Member.fromDoc(DocumentSnapshot doc) {
    return Member(
      id: doc.id,
      name: doc['name'],
      username: doc['username'],
      email: doc['email'],
      hpNum: doc['hpNum'],
      address: doc['address'],
      image: doc['image'],
      level: doc['level'],
      point: doc['point'],
      teamID: doc['teamID'],
    );
  }
}
