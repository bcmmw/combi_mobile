import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Activity {
  final String id;
  final String beforeImageUrl;
  final String afterImageUrl;
  final Timestamp timestamp;
  final int duration;
  final String memberId;

  Activity({
    this.id,
    this.beforeImageUrl,
    this.afterImageUrl,
    this.timestamp,
    this.duration,
    this.memberId,
  });

  factory Activity.fromDoc(DocumentSnapshot doc) {
    return Activity(
      id: doc.id,
      beforeImageUrl: doc['beforeImageUrl'],
      afterImageUrl: doc['afterImageUrl'],
      timestamp: doc['timestamp'],
      duration: doc['duration'],
      memberId: doc['memberId'],
    );
  }
}
