import 'package:cloud_firestore/cloud_firestore.dart';

class NotificationModel {
  String id;
  String type;
  String fromUserId;
  Timestamp timestamp;
  String feedID;

  NotificationModel(
      {this.id, this.type, this.fromUserId, this.timestamp, this.feedID});

  factory NotificationModel.fromDoc(DocumentSnapshot doc) {
    return NotificationModel(
        id: doc.id,
        type: doc['type'],
        fromUserId: doc['fromUserId'],
        timestamp: doc['timestamp'],
        feedID: doc['feedID']);
  }
}
