import 'package:cloud_firestore/cloud_firestore.dart';

class Event {
  String id;
  String name;
  String description;
  String address;
  String image;
  String teamID;
  int point;
  Timestamp startDateTime;
  Timestamp endDateTime;

  Event(
      {this.id,
      this.name,
      this.description,
      this.address,
      this.image,
      this.teamID,
      this.point,
      this.startDateTime,
      this.endDateTime});

  factory Event.fromDoc(DocumentSnapshot doc) {
    return Event(
      id: doc.id,
      name: doc['name'],
      address: doc['address'],
      description: doc['description'],
      image: doc['image'],
      teamID: doc['teamID'],
      point: doc['point'],
      startDateTime: doc['startDateTime'],
      endDateTime: doc['endDateTime'],
    );
  }
}
