import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:combi_mobile/controller/UserController.dart';
import 'package:combi_mobile/models/Member.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthService {
  static final _auth = FirebaseAuth.instance;
  static final _fireStore = FirebaseFirestore.instance;
  static String errorMsg;

  static Future<bool> signUp(Member user) async {
    try {
      UserCredential authResult = await _auth.createUserWithEmailAndPassword(
          email: user.email, password: user.password);

      User signedInUser = authResult.user;

      if (signedInUser != null) {
        _fireStore.collection('users').doc(signedInUser.uid).set({
          'username': user.username,
          'name': user.name,
          'email': user.email,
          'hpNum': user.hpNum,
          'address': user.address,
          'level': 1,
          'point': 0,
          'image': '',
          'type': 1,
          'teamID': '',
        });
        // _fireStore.collection('members').doc(signedInUser.uid).set({
        //   'username': username,
        //   'name': name,
        //   'email': email,
        //   'hpNum': hpNum,
        //   'address': address,
        // });
        return true;
      }

      return false;
    } catch (e) {
      print(e);
      errorMsg = getMessageFromErrorCode(e.code);
      return false;
    }
  }

  static Future<bool> login(String email, String password) async {
    try {
      await _auth.signInWithEmailAndPassword(email: email, password: password);
      return true;
    } catch (e) {
      print(e.code);
      errorMsg = getMessageFromErrorCode(e.code);
      print(errorMsg);
      return false;
    }
  }

  static void logout() {
    try {
      _auth.signOut();
    } catch (e) {
      print(e);
    }
  }

  static Future<UserCredential> reauthenticate(String password) async {
    var user = _auth.currentUser;

// Then use the newly re-authenticated user authResult.user
    try {
      UserCredential authResult = await user.reauthenticateWithCredential(
        EmailAuthProvider.credential(
          email: user.email,
          password: password,
        ),
      );
      return authResult;
    } catch (e) {
      print(e.code);
      errorMsg = getMessageFromErrorCode(e.code);
      print(errorMsg);
      return null;
    }
  }

  static Future<bool> updateEmail(String email) async {
    var user = _auth.currentUser;

    try {
      await user.updateEmail(email);
      await UserController.updateEmail(user.uid, email);
      return true;
    } catch (e) {
      print(e.code);
      errorMsg = getMessageFromErrorCode(e.code);
      print(errorMsg);
      return false;
    }
  }

  static Future<bool> updatePassword(String password) async {
    var user = _auth.currentUser;
// Then use the newly re-authenticated user authResult.user
    try {
      await user.updatePassword(password);
      return true;
    } catch (e) {
      print(e.code);
      errorMsg = getMessageFromErrorCode(e.code);
      print(errorMsg);
      return false;
    }
  }

  static String getMessageFromErrorCode(e) {
    switch (e) {
      case "ERROR_EMAIL_ALREADY_IN_USE":
      case "account-exists-with-different-credential":
      case "email-already-in-use":
        return "Email already used. Go to login page.";
        break;
      case "ERROR_WRONG_PASSWORD":
      case "wrong-password":
        return "Wrong email/password combination.";
        break;
      case "ERROR_USER_NOT_FOUND":
      case "user-not-found":
        return "No user found with this email.";
        break;
      case "ERROR_USER_DISABLED":
      case "user-disabled":
        return "User disabled.";
        break;
      case "ERROR_TOO_MANY_REQUESTS":
      case "operation-not-allowed":
        return "Too many requests to log into this account.";
        break;
      case "ERROR_OPERATION_NOT_ALLOWED":
      case "operation-not-allowed":
        return "Server error, please try again later.";
        break;
      case "ERROR_INVALID_EMAIL":
      case "invalid-email":
        return "Email address is invalid.";
        break;
      default:
        return "Unexpected Error: " + e.toString();
        break;
    }
  }
}
