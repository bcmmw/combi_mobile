import 'dart:math';

class LevelExpService {
  //Level needed for each level
  // LvL1: 50 Xp
  // Lvl2: 150 Xp
  // LvL3: 300 Xp
  // Lvl4: 500 Xp

// Calculate the total Exp needed to level up
  static int calculateLevelExp(int currentLevel) {
    return 25 * currentLevel * (1 + currentLevel);
  }

  //calculate user's current level
  static int calculateLevel(int currentXp) {
    int level = ((25 + sqrt(625 + 100 * currentXp)).floor() / 50).round();
    print((25 + sqrt(625 + 100 * currentXp)).floor() / 50);

    return level;
  }
}
